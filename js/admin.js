function setParam(id, str, event) {
    event.preventDefault();
    let value = document.getElementById(id).value
    if (value.length > 0) {
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                alert(str + " changed to " + value)
            }
        }
        let data = {}
        data[id] = value
        let json = JSON.stringify(data)
        xhr.open("PUT", "../../apps/uppush/" + id + "/", true)
        xhr.setRequestHeader('Content-type','application/json charset=utf-8')
        xhr.send(json)
    }
}

function addListeners() {
    const inputs = {
        "keepalive": "Keepalive",
        "max_message_ttl": "Max message TTL",
    };
    for(const id in inputs) {
        const message = inputs[id];
        const input = document.getElementById(id);
        if(input) {
            input.parentElement.addEventListener("submit", setParam.bind(null, id, message));
        }
    }
}

document.addEventListener('DOMContentLoaded', function() {
    if(document.getElementById("keepalive") == null) {
        window._nc_event_bus.subscribe('core:user-menu:mounted', addListeners);
    } else {
        addListeners();
    }
});
