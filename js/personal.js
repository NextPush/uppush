function expand(id) {
    let deviceId = id.split("toggle-")[1]
    document.getElementById("toggle-" + deviceId).innerHTML = "-"
    document.getElementById("toggle-" + deviceId).onclick = function() { shrink(id) }
    document.getElementById("table-" +  deviceId).removeAttribute("hidden")
}
function shrink(id) {
    let deviceId = id.split("toggle-")[1]
    document.getElementById("toggle-" + deviceId).innerHTML = "+"
    document.getElementById("toggle-" + deviceId).onclick = function() { expand(id) }
    document.getElementById("table-" +  deviceId).setAttribute("hidden","1")
}
function deleteDevice(id) {
    let deviceId = id.split("delete-")[1]
    if (confirm("Confirm to delete the device.")) {
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                    console.log(deviceId + " deleted")
                    document.getElementById("li-"+deviceId).remove()
                    document.getElementById("table-"+deviceId).remove()
            }
        }
        xhr.open("DELETE", "../../apps/uppush/device/" + deviceId, true)
        xhr.send()
    }
}
function deleteApp(id) {
    let appId = id.split("delete-")[1]
    let xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
                console.log(appId + " deleted")
                document.getElementById("tr-"+appId).remove()
        }
    }
    xhr.open("DELETE", "../../apps/uppush/app/" + appId, true)
    xhr.send()
}
document.addEventListener('DOMContentLoaded', function(){
    const addListener = () => {
        const root = document.getElementById("uppush-auth")
        root.querySelectorAll(".toggle-device").forEach((el) => {
            shrink(el.id)
        })
        root.querySelectorAll(".delete-device").forEach((el) => {
            el.onclick = function() {deleteDevice(el.id)}
        })
        root.querySelectorAll(".delete-app").forEach((el) => {
            el.onclick = function() {deleteApp(el.id)}
        })
        const f = new Intl.DateTimeFormat(navigator.language, {
            day: "numeric",
            month: "long",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        root.querySelectorAll(".uppush-date").forEach((el) => {
            const d = new Date(el.textContent.trim());
            el.textContent = f.format(d);
            el.classList.add("setted");
        })
    }
    if (document.querySelectorAll(".toggle-device")) {
        addListener()
    } else {
        window._nc_event_bus.subscribe('core:user-menu:mounted', addListener)
    }
})

