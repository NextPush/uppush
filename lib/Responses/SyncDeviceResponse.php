<?php
declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Responses;

use OCA\UnifiedPushProvider\Device\Device;

use OCP\AppFramework\Http\ICallbackResponse;
use OCP\AppFramework\Http\IOutput;
use OCP\AppFramework\Http\Response;
use OC\AppFramework\Http;
use OCA\UnifiedPushProvider\Request\RequestSync;
use Psr\Container\ContainerInterface;

class SyncDeviceResponse extends Response implements ICallbackResponse
{
    /** @var ContainerInterface */
    private ContainerInterface $context;
    /** @var RequestSync */
    private RequestSync $syncConfig;
    /** @var string */
    private string $deviceId;

    public function __construct(
        ContainerInterface $context,
        RequestSync $syncConfig,
        string $deviceId
    ) {
        parent::__construct(Http::STATUS_OK, array(
            'Cache-Control' => 'no-cache',
            'X-Accel-Buffering' => 'no',
            'Content-Type' => 'text/event-stream',
        ));
        $this->context = $context;
        $this->syncConfig = $syncConfig;
        $this->deviceId = $deviceId;
    }

    public function callback(IOutput $output)
    {
        Device::withDevice($this->context, $this->deviceId, function (Device $device) {
            $device->sync($this->syncConfig);
        });
    }
}
