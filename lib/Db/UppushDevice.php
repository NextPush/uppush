<?php

namespace OCA\UnifiedPushProvider\Db;

use DateTimeImmutable;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use OCA\UnifiedPushProvider\Utils;

use OCP\IDBConnection;
use OCP\IUserSession;

class UppushDevice
{
    /** @var IDBConnection */
    private IDBConnection $db;
    /** @var IUserSession */
    private IUserSession $userSession;

    /**
     * Constructor for UppushDevice.
     *
     * @param IDBConnection $db The database connection to use.
     * @param IUserSession $userSession The user session to use.
     */
    function __construct(
        IDBConnection $db,
        IUserSession $userSession
    ) {
        $this->db = $db;
        $this->userSession = $userSession;
    }

    public function create(string $deviceName): ?string
    {
        $deviceId = Utils::uuid();
        $t = new DateTimeImmutableType();
        $d = $t->convertToDatabaseValue(new DateTimeImmutable(), $this->db->getDatabasePlatform());
        $query = $this->db->getQueryBuilder();
        $query->insert('uppush_devices')
            ->values([
                'user_id' => $query->createNamedParameter($this->userSession->getUser()->getUID()),
                'device_id' => $query->createNamedParameter($deviceId),
                'device_name' => $query->createNamedParameter($deviceName),
                'date' => $query->createNamedParameter($d)
            ]);
        if ($query->execute() === 1) {
            return $deviceId;
        } else {
            return null;
        }
    }

    /**
     * Delete a device by its device ID.
     *
     * @param string $deviceId The ID of the device to delete.
     * @return bool
     */
    public function delete(string $deviceId): bool
    {
        $query = $this->db->getQueryBuilder();
        $query->delete('uppush_devices')
            ->where($query->expr()->eq('device_id', $query->createNamedParameter($deviceId)));
        return $query->execute() === 1;
    }

    /**
     * Delete a device by its user ID.
     *
     * @param string $userId The ID of the user to delete.
     * @return void
     */
    public function deleteUser(string $userId): void
    {
        $query = $this->db->getQueryBuilder();
        $query->delete('uppush_devices')
            ->where($query->expr()->eq('user_id', $query->createNamedParameter($userId)));
        $query->execute();
    }

    /**
     * Retrieve a list of all devices IDs from uppush_devices.
     *
     * @return string[] An array of device IDs.
     */
    public function getAlldevices(): array {
        $query = $this->db->getQueryBuilder();
        $query->select('device_id')
            ->from('uppush_devices');
        $result = $query->execute();
        $userIds = array();
        while ($row = $result->fetch()) {
            array_push($userIds, $row['device_id']);
        }
        $result->closeCursor();
        return $userIds;
    }

    /**
     * Retrieve a list of all distinct user IDs from uppush_devices.
     *
     * @return string[] An array of user IDs.
     */
    public function getAllUsers(): array {
        $query = $this->db->getQueryBuilder();
        $query->selectDistinct('user_id')
            ->from('uppush_devices');
        $result = $query->execute();
        $userIds = array();
        while ($row = $result->fetch()) {
            array_push($userIds, $row['user_id']);
        }
        $result->closeCursor();
        return $userIds;
    }

    /**
     * Check if a device id exists
     * @param string $deviceId
     * @return bool
     */
    public function exists(string $deviceId): bool
    {
        $query = $this->db->getQueryBuilder();
        $query->select('device_id')
            ->from('uppush_devices')
            ->where($query->expr()->eq('device_id', $query->createNamedParameter($deviceId)));

        $result = $query->execute();
        $resultId = null;
        if ($row = $result->fetch()) {
            $resultId = $row['device_id'];
        }
        return ($resultId !== null);
    }
}
