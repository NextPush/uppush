<?php

namespace OCA\UnifiedPushProvider\Db;

use OCP\IDBConnection;
use Exception;

class UppushConfig
{
    /**
     * @var int Default value for max TTL for messages, 1 day
     */
    private const maxMessageTtlSec = 86400; // 24 * 60 * 60

    /**
     * @var int|null
     */
    private ?int $maxMessageTtl = null;

    /**
     * @var int|null
     */
    private ?int $keepalive = null;

    /** @var IDBConnection */
    private IDBConnection $db;

    /**
     * @param IDBConnection $db The database connection to use.
     */
    public function __construct(IDBConnection $db) {
        $this->db = $db;
    }

    /**
     * Get a configuration parameter from the database.
     *
     * @param string $name The name of the configuration parameter.
     * @param mixed $defaultValue The value to return if the configuration parameter is not set.
     *
     * @return mixed The value of the configuration parameter, or the default value.
     */
    private function getConfigParameter(string $name, $defaultValue)
    {
        $query = $this->db->getQueryBuilder();
        $query->select('value')
            ->from('uppush_config')
            ->where($query->expr()->eq('parameter', $query->createNamedParameter($name)));
        $result = $query->execute();
        $res = $defaultValue;
        if ($row = $result->fetch()) {
            $res = $row['value'];
        }
        $result->closeCursor();
        return $res;
    }

    /**
     * Sets a configuration parameter in the database.
     *
     * @param string $name The name of the configuration parameter.
     * @param mixed $value The value to set for the configuration parameter.
     */
    private function setConfigParameter(string $name, $value)
    {
        // TODO use upsert request if available one day
        try {
            $query = $this->db->getQueryBuilder();
            $query->delete('uppush_config')
                ->where($query->expr()->eq('parameter', $query->createNamedParameter($name)));
            $query->execute();
        } catch (Exception $ex) {
        }
        $query = $this->db->getQueryBuilder();
        $query->insert('uppush_config')
            ->values([
                'parameter' => $query->createNamedParameter($name),
                'value' => $query->createNamedParameter($value)
            ]);
        $query->execute();
    }

    /**
     * Get the maximum Time To Live (TTL) value for a message.
     * 
     * @return int The maximum TTL value in seconds.
     */
    public function getMaxMessageTtl(): int
    {
        if ($this->maxMessageTtl === null) {
            $this->maxMessageTtl = (int)$this->getConfigParameter("max_message_ttl", Self::maxMessageTtlSec);
        }
        return $this->maxMessageTtl;
    }

    /**
     * Get the keepalive time in seconds.
     * 
     * @return int The keepalive time in seconds.
     */
    public function getKeepalive(): int
    {
        if ($this->keepalive === null) {
            $this->keepalive = (int)$this->getConfigParameter('keepalive', 50);
        }
        return $this->keepalive;
    }

    /**
     * @param int $keepalive
     *
     * @return bool
     */
    public function setKeepalive(int $keepalive): bool
    {
        if ($keepalive < 15 || $keepalive > 600) {
            return false;
        }
        $this->setConfigParameter('keepalive', $keepalive);
        $this->keepalive = $keepalive;
        return true;
    }

    /**
     * @param int $max_message_ttl the max message ttl
     *
     * @return bool
     */
    public function setMaxMessageTtl(int $max_message_ttl): bool
    {
        if ($max_message_ttl < 3600 || $max_message_ttl > 259200 /* 3 * 24 * 60 * 60*/) {
            return false;
        }
        $this->setConfigParameter('max_message_ttl', $max_message_ttl);
        $this->maxMessageTtl = $max_message_ttl;
        return true;
    }
}
