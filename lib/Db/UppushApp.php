<?php

namespace OCA\UnifiedPushProvider\Db;

use DateTimeImmutable;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use OCA\UnifiedPushProvider\Device\SubscriptionInfo;
use OCA\UnifiedPushProvider\Utils;

use OCP\IDBConnection;
use Psr\Container\ContainerInterface;

class UppushApp
{
    /** @var IDBConnection */
    private IDBConnection $db;
    /** @var ContainerInterface */
    private ContainerInterface $context;

    /**
     * Constructor for UppushDevice.
     *
     * @param IDBConnection $db The database connection to use
     * @param ContainerInterface $context
     */
    function __construct(
        IDBConnection $db,
        ContainerInterface $context
    ) {
        $this->db = $db;
        $this->context = $context;
    }

    /**
     * Create an authorization token for a new 3rd party service.
     *
     * @param string $deviceId The ID of the device for which the token is created.
     * @param string $appName The name of the 3rd party service.
     * @param ?string $vapid The VAPID public key of the 3rd party service, or null if not applicable.
     * @return ?string The authorization token created, or null if the creation failed.
     */
    public function create(
        string $deviceId,
        string $appName,
        ?string $vapid
    ): ?string {
        $token = Utils::uuid();

        $t = new DateTimeImmutableType();
        $d = $t->convertToDatabaseValue(new DateTimeImmutable(), $this->db->getDatabasePlatform());
        $query = $this->db->getQueryBuilder();
        $query->insert('uppush_applications')
            ->values([
                'device_id' => $query->createNamedParameter($deviceId),
                'app_name' => $query->createNamedParameter($appName),
                'token' => $query->createNamedParameter($token),
                'date' => $query->createNamedParameter($d),
                'vapid_pubkey' => $query->createNamedParameter($vapid),
            ]);
        if ($query->execute() === 1) {
            return $token;
        } else {
            return null;
        }
    }

    /**
     * @param string $token
     * @return bool
     */
    public function delete(string $token): bool
    {
        $query = $this->db->getQueryBuilder();
        $query->delete('uppush_applications')
            ->where($query->expr()->eq('token', $query->createNamedParameter($token)));
        return $query->execute() === 1;
    }

    /**
     * Retrieves the subscrition info associated with a given token.
     *
     * @param string $token The authorization token.
     * @return SubscriptionInfo|null The subscrition info if found, otherwise null.
     */
    public function getInfos(string $token): ?SubscriptionInfo
    {
        $query = $this->db->getQueryBuilder();
        $query->select('device_id', 'vapid_pubkey', 'vapid_jwt_cache', 'vapid_jwt_cache_expire')
            ->from('uppush_applications')
            ->where($query->expr()->eq('token', $query->createNamedParameter($token)));
        $result = $query->execute();
        $deviceId = null;
        $vapidPubkey = null;
        $vapidLastJWT = null;
        $vapidJwtExpire = 0;
        if ($row = $result->fetch()) {
            $deviceId = $row['device_id'];
            $vapidPubkey = $row['vapid_pubkey'];
            $vapidLastJWT = $row['vapid_jwt_cache'];
            $vapidJwtExpire = $row['vapid_jwt_cache_expire'];
        }
        if ($deviceId == null) {
            return null;
        }
        return new SubscriptionInfo(
            $this->context,
            $this->db,
            $token,
            $deviceId,
            $vapidPubkey,
            $vapidLastJWT,
            (int)$vapidJwtExpire,
        );
    }
}
