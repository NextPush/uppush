<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Request;

use OCA\UnifiedPushProvider\Db\UppushConfig;
use OCA\UnifiedPushProvider\Device\IdByUrgency;
use OCA\UnifiedPushProvider\Device\Urgency;

class RequestSync
{
    /**
     * @var int
     */
    public int $urgency;

    /**
     * @var int
     */
    public int $keepalive;

    /**
     * @var IdByUrgency
     */
    public IdByUrgency $lastEventIds;


    /**
     * @param UppushConfig $config
     * @param RequestUrgency $urgency
     */
    public function __construct(
        UppushConfig $config,
        RequestUrgency $urgency,
        RequestLastEventId $lastEventIds
    ) {
        $this->urgency = $urgency->get(Urgency::VeryLow);
        $this->keepalive = $config->getKeepalive();
        $this->lastEventIds = $lastEventIds->lastEventIds;
    }
}
