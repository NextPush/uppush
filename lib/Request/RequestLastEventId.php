<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Request;

use OCA\UnifiedPushProvider\Device\IdByUrgency;
use OCP\IRequest;

class RequestLastEventId
{
    /**
     * @var IdByUrgency
     */
    public IdByUrgency $lastEventIds;

    /**
     * @param IRequest $request The request object containing headers
     */
    function __construct(IRequest $request)
    {
        $lastEventId = $request->getHeader("last-event-id");
        if (empty($lastEventId)) {
            $this->lastEventIds = new IdByUrgency(null);
        } else {
            $this->lastEventIds = new IdByUrgency($lastEventId);
        }
    }
}
