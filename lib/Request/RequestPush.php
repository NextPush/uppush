<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Request;

use OCA\UnifiedPushProvider\Device\Urgency;

class RequestPush
{
    /**
     * @var int
     */
    public int $ttl;

    /**
     * @var int
     */
    public int $urgency;

    /**
     * @var string
     */
    public ?string $topic;

    /**
     * @var string
     */
    public ?string $authorization;

    /**
     * @param ?string $s
     * @return ?string
     */
    private static function maybeTrim(?string $s): ?string {
        return empty($s) ? $s : trim($s);
    }

    /**
     * @param RequestUrgency $urgency
     * @param RequestTtl $ttl
     * @param RequestTopic $topic
     * @param RequestAuthorization $auth
     */
    public function __construct(
        RequestUrgency $urgency,
        RequestTtl $ttl,
        RequestTopic $topic,
        RequestAuthorization $auth
    ) {
        $this->ttl = $ttl->ttl;
        $this->urgency = $urgency->get(Urgency::Normal);
        $this->topic = Self::maybeTrim($topic->topic);
        $this->authorization = Self::maybeTrim($auth->authorization);
    }
}
