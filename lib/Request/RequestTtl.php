<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Request;

use OCA\UnifiedPushProvider\Db\UppushConfig;

use OCP\IRequest;

class RequestTtl
{
    /**
     * The TTL of the message in seconds
     *
     * @var int
     */
    public int $ttl;

    /**
     * Constructor for RequestTtl class.
     *
     * @param IRequest $request The request object containing headers.
     * @param UppushConfig $config The configuration object for TTL settings.
     */
    function __construct(IRequest $request, UppushConfig $config)
    {
        $maxMessageTtl = $config->getMaxMessageTtl();
        $ttl = $request->getHeader("ttl");
        if (is_numeric($ttl)) {
            $this->ttl = min($maxMessageTtl, (int)$ttl);
        } else {
            $this->ttl = $maxMessageTtl;
        }
    }
}
