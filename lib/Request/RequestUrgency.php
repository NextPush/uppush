<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Request;

use OCA\UnifiedPushProvider\Device\Urgency;
use OCP\IRequest;

class RequestUrgency
{
    /**
     * The urgency of the request.
     *
     * @var int|null
     */
    private ?int $urgency;

    /**
     * Constructor for RequestUrgency.
     *
     * @param IRequest $request The request object containing headers.
     */
    function __construct(IRequest $request)
    {
        $urgency = $request->getHeader("urgency");
        $this->urgency = Urgency::fromString($urgency);
    }

    /**
     * @param int $default The default urgency to return if none is set
     *
     * @return int The urgency of the request, or the default if none is set
     */
    public function get(int $default): int
    {
        return $this->urgency === null ? $default : $this->urgency;
    }
}
