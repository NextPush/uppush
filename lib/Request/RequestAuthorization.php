<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Request;

use OCP\IRequest;

class RequestAuthorization
{
    /**
     * @var string|null The authorization header
     */
    public ?string $authorization;

    /**
     * @param IRequest $request The request object containing headers
     */
    function __construct(IRequest $request)
    {
        $this->authorization = $request->getHeader("authorization");
    }
}
