<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Request;

use OCP\IRequest;

class RequestTopic
{
    /**
     * @var string|null The topic associated with the request
     */
    public ?string $topic;

    /**
     * @param IRequest $request The request object containing headers
     */
    function __construct(IRequest $request)
    {
        $this->topic = $request->getHeader("topic");
    }
}
