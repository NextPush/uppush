<?php

namespace OCA\UnifiedPushProvider\Migration;

use OC\RedisFactory;
use OCP\App\IAppManager;
use OCP\Migration\IOutput;
use OCP\Migration\IRepairStep;

class Installation implements IRepairStep
{
    /** @var IAppManager */
    private IAppManager $appManager;
    /** @var RedisFactory */
    private RedisFactory $redisFactory;
    public function __construct(
        IAppManager $appManager,
        RedisFactory $redisFactory
    ) {
        $this->appManager = $appManager;
        $this->redisFactory = $redisFactory;
    }

    public function getName()
    {
        return 'uppush';
    }

    public function run(IOutput $output)
    {
        $version = $this->appManager->getAppInfo("uppush")["version"];
        $redis = $this->redisFactory->getInstance();
        try {
            $redis->set("uppush.version", $version);
        } finally {
            $redis->close();
        }
    }
}
