<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Migration;

use Closure;
use Exception;
use DateTimeImmutable;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Types\Types;
use Doctrine\DBAL\Types\DateTimeImmutableType;
use OCA\UnifiedPushProvider\Utils;
use OCP\DB\ISchemaWrapper;
use OCP\IDBConnection;
use OCP\Migration\IOutput;
use OCP\Migration\SimpleMigrationStep;
use Psr\Log\LoggerInterface;

/**
This migration creates the table if needed, else tries to fix the setups:
The first migration step is to create new tables with _n suffix,
and copy data from the previous to the new.
The 2nd step rename new _n to the correct name.
*/
class Version020205 extends SimpleMigrationStep {
	/** @var IDBConnection */
	private IDBConnection $db;

	function __construct(LoggerInterface $logger, IDBConnection $db) {
		$this->db = $db;
	}

	function log($val): void {
		echo $val . PHP_EOL;
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function preSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();
		$this->db->dropTable('uppush_config_n');
		$this->db->dropTable('uppush_applications_n');
		$this->db->dropTable('uppush_devices_n');
		// Try dropping index directly
		try {
			$this->dropIndex('uppush_device_id');
			$this->dropIndex('uppush_app_device_id');
			$this->dropIndex('uppush_user_id');
			$this->dropIndex('uppush_token');
			$this->dropIndex('uppush_parameter');
		} catch (Exception $e) {
		    $this->log("Couldn't drop index directly. Trying with abstract now.");
		}
		// Try dropping with abstracts
		if ($schema->hasTable('uppush_devices')) {
			$table = $schema->getTable('uppush_devices');
			try {
				$table->dropIndex('uppush_device_id');
			} catch(Exception $e) {}
			try {
				$table->dropIndex('uppush_user_id');
			} catch(Exception $e) {}
		}
		if ($schema->hasTable('uppush_applications')) {
			$table = $schema->getTable('uppush_applications');
			try {
				$table->dropIndex('uppush_app_device_id');
			} catch(Exception $e) {}
			try {
				$table->dropIndex('uppush_token');
			} catch(Exception $e) {}
		}
		if ($schema->hasTable('uppush_config')) {
			$table = $schema->getTable('uppush_config');
			try {
				$table->dropIndex('uppush_parameter');
			} catch(Exception $e) {}
		}
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 * @return null|ISchemaWrapper
	 */
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options): ?ISchemaWrapper {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();

		if ($schema->hasTable('uppush_devices')) {
			$this->log("Table uppush_devices already exists.");
			$devicesTable = $this->createDevicesTable($schema, '_n');
		} else {
			$this->log("Creating table uppush_devices.");
			$devicesTable = $this->createDevicesTable($schema);
		}

		if ($schema->hasTable('uppush_applications')) {
			$this->log("Table uppush_applications already exists.");
			$this->createApplicationsTable($schema, $devicesTable, '_n');
		} else {
			$this->log("Creating table uppush_applications.");
			$this->createApplicationsTable($schema, $devicesTable);
		}

		if ($schema->hasTable('uppush_config')) {
			$this->log("Table uppush_config already exists.");
			$this->createConfigTable($schema, '_n');
		} else {
			$this->log("Creating table uppush_config.");
			$this->createConfigTable($schema);
		}
		return $schema;
	}

	function copyTable(ISchemaWrapper $schema, string $from, string $to, array $columns, string $condition = ''): void {
		$selColumns = [];
		$vals = [];
		$oldTable = $schema->getTable($from);
		$now = "'".(new DateTimeImmutableType())->convertToDatabaseValue(new DateTimeImmutable(), $this->db->getDatabasePlatform())."'";
		foreach ($columns as $k => $col) {
			if ($oldTable->hasColumn($col)) {
				array_push($selColumns, $col);
				if ($col === 'date') {
					if (!($oldTable->getColumn('date')->getType() instanceof \Doctrine\DBAL\Types\DateTimeImmutableType)) {
						array_push($vals, $now);
					} else {
						array_push($vals, $col);
					}
				} else {
					array_push($vals, $col);
				}
			}
		}
		$to = $this->db->getQueryBuilder()->getTableName($to);
		$from = $this->db->getQueryBuilder()->getTableName($from);
		$sql = "INSERT INTO " . $to . "(" . join(',', $selColumns) . ")
			SELECT " . join(',', $vals) . "
			FROM " . $from . " " .$condition;
		if (method_exists($this->db, 'executeStatement')) {
			$this->db->executeStatement($sql);
		} else {
			$this->db->executeUpdate($sql);
		}
	}

	function copyDates(ISchemaWrapper $schema, string $from, string $to, string $id): void {
		$selColumns = [];
		$oldTable = $schema->getTable($from);
		if (!($oldTable->getColumn('date')->getType() instanceof \Doctrine\DBAL\Types\DateTimeImmutableType)) {
			try {
				$type = new DateTimeImmutableType;
				$query = $this->db->getQueryBuilder();
				$query->select($id, 'date')->from($from);
				$r = $query->execute();
				while ($row = $r->fetch()) {
					$ds = $row['date'];
					$d = new DateTimeImmutable($ds);
					$uquery = $this->db->getQueryBuilder();
					$uquery
						->update($to)
						->set('date', $uquery->createNamedParameter(
							$type->convertToDatabaseValue($d, $this->db->getDatabasePlatform())
						))
						->where($uquery->expr()->eq($id, $uquery->createNamedParameter($row[$id])))
						->execute();
				}
				$r->closeCursor();
			} catch (\Exception $e) {
				// ignore
			}
		}
	}

	function renameTable(string $from, string $to): void {
		$this->log("Renaming " . $from);
		$this->db->dropTable($to);
		$this->log($to . " dropped");
		$to = $this->db->getQueryBuilder()->getTableName($to);
		$from = $this->db->getQueryBuilder()->getTableName($from);
		$sql = "ALTER TABLE " . $from . " RENAME TO " . $to;
		if (method_exists($this->db, 'executeStatement')) {
			$this->db->executeStatement($sql);
		} else {
			$this->db->executeUpdate($sql);
		}
	}

	function dropIndex(string $index): void {
		$sql = "DROP INDEX IF EXISTS " . $index;
		if (method_exists($this->db, 'executeStatement')) {
			$this->db->executeStatement($sql);
		} else {
			$this->db->executeUpdate($sql);
		}
	}

	function createDevicesTable(ISchemaWrapper $schema, string $suffix = ''): Table {
		$devicesTable = $schema->createTable('uppush_devices' . $suffix);
		$devicesTable->addColumn('user_id', Types::STRING, [
			'notnull' => true,
		]);
		$devicesTable->addColumn('device_id', Types::STRING, [
			'notnull' => true,
			'length' => Utils::DB_IDS_LENGTH,
		]);
		$devicesTable->addColumn('device_name', Types::STRING, [
			'notnull' => true,
		]);
		$devicesTable->addColumn('date', Types::DATETIME_IMMUTABLE, [
			'notnull' => true,
		]);
		$devicesTable->setPrimaryKey(['device_id'], 'uppush_device_id');
		$devicesTable->addIndex(['user_id'], 'uppush_user_id');
		return $devicesTable;
	}

	function createApplicationsTable(ISchemaWrapper $schema, Table $devicesTable, string $suffix = ''): void {
		$appTable = $schema->createTable('uppush_applications' . $suffix);
		$appTable->addColumn('device_id', Types::STRING, [
			'notnull' => true,
			'length' => Utils::DB_IDS_LENGTH,
		]);
		$appTable->addColumn('app_name', Types::STRING, [
			'notnull' => true,
		]);
		$appTable->addColumn('token', Types::STRING, [
			'notnull' => true,
			'length' => Utils::DB_IDS_LENGTH,
		]);
		$appTable->addColumn('date', Types::DATETIME_IMMUTABLE, [
			'notnull' => true,
		]);
		$appTable->addColumn('vapid_pubkey', Types::STRING, [
			'notnull' => false,
		]);
		$appTable->addColumn('vapid_jwt_cache', Types::STRING, [
			'notnull' => false,
		]);
		$appTable->addColumn('vapid_jwt_cache_expire', Types::BIGINT, [
			'default' => 0,
			'notnull' => true,
		]);
		$appTable->setPrimaryKey(['token'], 'uppush_token');
		$appTable->addIndex(['device_id'], 'uppush_app_device_id');
		$fk = "uppush_device_id_fk";
		if ($schema->hasTable('uppush_applications')) {
			$table = $schema->getTable('uppush_applications');
			if ($schema->getTable('uppush_applications')->hasForeignKey($fk)) {
				$fk = $fk . "_1";
			}
		}
		$this->log("New apps table, using FK: " . $fk);
		$appTable->addForeignKeyConstraint(
			$devicesTable,
			['device_id'],
			['device_id'],
			["onDelete" => "CASCADE"],
			$fk
		);
	}

	function createConfigTable(ISchemaWrapper $schema, string $suffix = ''): void {
		$table = $schema->createTable('uppush_config' . $suffix);
		$table->addColumn('parameter', Types::STRING, [
			'notnull' => true,
		]);
		$table->addColumn('value', Types::STRING, [
			'notnull' => true,
		]);
		$table->setPrimaryKey(['parameter'], 'uppush_parameter');
	}


	/**
	 * First copy data from old to new (device before applications because of constraints)
	 * Then replace old by new (DROP applications before device because of constraints)
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function postSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();

		if ($schema->hasTable('uppush_config_n')) {
			$this->log("Table uppush_config_n found");
			$this->copyTable($schema, 'uppush_config', 'uppush_config_n', [
				'parameter',
				'value',
			]);
		} else {
			$this->log("Table uppush_config_n not found");
		}
		if ($schema->hasTable('uppush_devices_n')) {
			$this->log("Table uppush_devices_n found");
			$this->copyTable($schema, 'uppush_devices', 'uppush_devices_n', [
				'user_id',
				'device_id',
				'date',
				'device_name',
			]);
		    $this->copyDates($schema, 'uppush_devices', 'uppush_devices_n', 'device_id');
		} else {
			$this->log("Table uppush_devices_n not found");
		}
		if ($schema->hasTable('uppush_applications_n')) {
			$this->log("Table uppush_applications_n found");
			$devicesTable = $this->db->getQueryBuilder()->getTableName('uppush_devices');
			$this->copyTable($schema, 'uppush_applications', 'uppush_applications_n', [
				'device_id',
				'app_name',
				'token',
				'date',
				'vapid_pubkey',
				'vapid_jwt_cache',
				'vapid_jwt_cache_expire',
			], 'WHERE device_id in (SELECT device_id FROM ' . $devicesTable .')');
		    $this->copyDates($schema, 'uppush_applications', 'uppush_applications_n', 'token');
		} else {
			$this->log("Table uppush_applications_n not found");
		}
		// Drop foreign key
		if ($schema->hasTable('uppush_applications')) {
			$table = $schema->getTable('uppush_applications');
			$fks = $table->getForeignKeys();
			foreach($fks as $fk) {
				$this->log("Dropping FK " . $fk->getName());
				try {
					$table->removeForeignKey($fk->getName());
				} catch (Exception $e) {
					$this->log("An error occured: " . $e->getMessage());
				}
			}
		}
		// DROP old and rename _n to the actual name
		if ($schema->hasTable('uppush_config_n')) {
			$this->renameTable('uppush_config_n', 'uppush_config');
		}
		if ($schema->hasTable('uppush_applications_n')) {
			$this->renameTable('uppush_applications_n', 'uppush_applications');
		}
		if ($schema->hasTable('uppush_devices_n')) {
			$this->renameTable('uppush_devices_n', 'uppush_devices');
		}
	}
}
