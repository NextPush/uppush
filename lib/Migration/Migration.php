<?php

namespace OCA\UnifiedPushProvider\Migration;

use OC\RedisFactory;
use OCP\App\IAppManager;
use OCP\IDBConnection;
use OCP\Migration\IOutput;
use OCP\Migration\IRepairStep;

use Redis;

class Migration implements IRepairStep
{
    /** @var IAppManager */
    private IAppManager $appManager;
    /** @var IDBConnection */
    private IDBConnection $db;
    /** @var RedisFactory */
    private RedisFactory $redisFactory;

    /** 
     * @param IAppManager $appManager
     * @param IDBConnection $db
     * @param RedisFactory $redisFactory
     */
    public function __construct(
        IAppManager $appManager,
        IDBConnection $db,
        RedisFactory $redisFactory
    ) {
        $this->appManager = $appManager;
        $this->db = $db;
        $this->redisFactory = $redisFactory;
    }

    public function getName(): string
    {
        return 'uppush';
    }

    /**
	 * Generates a random UUID
	 * @return string
	 */
	private static function uuid(): string {
		return str_replace('=', '', strtr(base64_encode(random_bytes(20)), '+/', '-_'));
	}

    /**
     * Close 1.* sync services running to ensure that it will run new code,
     * and migrate events to new 2.0 redis keys
     * @param Redis $redis
     */
    private function migrateTo200(Redis $redis)
    {
        $query = $this->db->getQueryBuilder();
        $query->select('device_id')
            ->from('uppush_devices');

        $result = $query->execute();
        $deviceIds = array();
        // get all devices, and ask shutdown of sync services
        while ($row = $result->fetch()) {
            $deviceId = $row['device_id'];
            $redis->lPush($deviceId, "shutdown_0");
            array_push($deviceIds, $deviceId);
        }
        // wait for shutdown
        usleep(500000);
        // migrate all devices
        foreach ($deviceIds as $deviceId) {
            $messageBase = "uppush.$deviceId.message.";
            $listKey = "uppush.$deviceId.messages.normal";
            while($event = $redis->lPop($deviceId)) {
                if (!(is_string($event) && substr($event, 0, 8) === 'shutdown')) {
                    $messageId = Self::uuid();
                    $multi = $redis->multi();
                    // Max 3 days
                    $multi->set($messageBase . $messageId, $event, 259200);
                    $multi->rPush($listKey, $messageId);
                    $multi->exec();
                }
            }
        }
    }

    /**
     * Close all sync services running to ensure that it will run new code
     * @param Redis $redis
     */
    private function closeSyncServices200(Redis $redis)
    {
        $query = $this->db->getQueryBuilder();
        $query->select('device_id')
            ->from('uppush_devices');

        $result = $query->execute();
        while ($row = $result->fetch()) {
            $deviceId = $row['device_id'];
            $multi = $redis->multi();
            $multi->set("uppush.$deviceId.owner", "");
            $multi->rPush("uppush.$deviceId.popped_messages.high", "close");
            $multi->exec();
        }
    }

    public function run(IOutput $output)
    {
        $version = $this->appManager->getAppInfo("uppush")["version"];
        $redis = $this->redisFactory->getInstance();
        try {
            $oldVersion = $redis->get("uppush.version");
            if (empty($oldVersion) || substr($oldVersion, 0, 1) === "1") {
                // migrate before 2.0.0
                // $output->debug("Migrating from pre 2.0.0");
                $this->migrateTo200($redis);
            } else {
                // migrate after 2.0.0
                // $output->debug("Migrating after 2.0.0");
                $this->closeSyncServices200($redis);
            }
            $redis->set("uppush.version", $version);
        } finally {
            $redis->close();
        }
    }
}
