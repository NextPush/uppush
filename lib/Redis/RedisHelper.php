<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Redis;

use OCA\UnifiedPushProvider\Redis\RedisFunction;
use OCA\UnifiedPushProvider\Redis\RedisCapacity;
use OCA\UnifiedPushProvider\Exceptions\UnsupportedRedis;

use Redis;
use OC\RedisFactory;
use OC\SystemConfig;
use OCP\Diagnostics\IEventLogger;
use Psr\Container\ContainerInterface;


class RedisHelper
{
    /** @var Redis */
    public Redis $redis;
    /** @var ContainerInterface */
    private ContainerInterface $context;
    /** @var RedisCapacity */
    private RedisCapacity $capacity;
    /** @var bool */
    private bool $isBase;
    /**
     * Optional second redis connexion to manage multi
     * @var Redis|null
     */
    private ?Redis $base = null;

    /**
     * @param Redis $redis
     * @param ContainerInterface $context
     * @param RedisCapacity $capacity
     * @param bool $isBase
     */
    public function __construct(
        Redis $redis,
        ContainerInterface $context,
        RedisCapacity $capacity,
        bool $isBase
    ) {
        $this->redis = $redis;
        $this->context = $context;
        $this->capacity = $capacity;
        $this->isBase = $isBase;
    }

    
    /**
     * This method must should be called by Factory
     * @param ContainerInterface $context
     * @return RedisHelper
     */
    public static function create(ContainerInterface $context): RedisHelper
    {
        $redis = $context->get(RedisFactory::class)->getInstance();
        $capacity = new RedisCapacity($redis);
        if (!$capacity->has(RedisFunction::BLPOP)) {
            throw new UnsupportedRedis();
        }
        return new RedisHelper(
            $redis,
            $context,
            $capacity,
            true,
        );
    }

    /**
     * Run a callback with RedisHelper instance
     * @param ContainerInterface $context
     * @param callable(RedisHelper):mixed $callback
     * @return mixed
     */
    public static function withHelper(ContainerInterface $context, callable $callback)
    {
        /** @var RedisHelper|null */
        $helper = null;
        try {
            $helper = $context->get(RedisHelper::class);
            return $callback($helper);
        } finally {
            if ($helper != null) {
                $helper->close();
            }
        }
    }

    function __destruct()
    {
        $this->close();
    }

    public function close()
    {
        if ($this->base != null) {
            $this->base->close();
            $this->base = null;
        }
        if ($this->isBase) {
            $this->redis->close();
        }
    }

    private function doReturn($value)
    {
        if ($value instanceof Redis) {
            return new Self($value, $this->context, $this->capacity, false);
        } else {
            return $value;
        }
    }

    /**
     * @return Self|string|bool
     */
    public function set(string $key, $value, $options = null)
    {
        return $this->doReturn($this->redis->set($key, $value, $options));
    }

    public function get(string $key)
    {
        return $this->doReturn($this->redis->get($key));
    }

    public function mget(array $keys)
    {
        return $this->doReturn($this->redis->mget($keys));
    }

    /**
     * @param array|string $key
     * @return Self|int|false
     */
    public function unlink($key, string ...$other_keys)
    {
        $res = null;
        if ($this->capacity->has(RedisFunction::UNLINK)) {
            $res = $this->redis->unlink($key, ...$other_keys);
        } else {
            $res = $this->redis->del($key, ...$other_keys);
        }
        return $this->doReturn($res);
    }

    public function keys(string $pattern)
    {
        return $this->doReturn($this->redis->keys($pattern));
    }

    /**
     * @return bool|Self
     */
    public function multi(int $value = Redis::MULTI)
    {
        if ($this->capacity->has(RedisFunction::MULTI)) {
            $multi = $this->redis->multi();
        } else {
            $multi = $this->redis->pipeline();
        }
        return $this->doReturn($multi);
    }

    /**
     * @return bool|Self
     */
    public function pipeline()
    {
        return $this->doReturn($this->redis->pipeline());
    }

    /**
     * @return Self|array|false
     */
    public function exec()
    {
        $res = $this->doReturn($this->redis->exec());
        $this->close();
        return $res;
    }

    /**
     * @param array|string $key
     * @return Self|bool
     */
    public function watch($key, ...$other_keys)
    {
        if ($this->capacity->has(RedisFunction::WATCH)) {
            return $this->doReturn($this->redis->watch($key, ...$other_keys));
        } else {
            return true;
        }
    }

    public function unwatch()
    {
        if ($this->capacity->has(RedisFunction::WATCH)) {
            return $this->doReturn($this->redis->unwatch());
        } else {
            return true;
        }
    }


    public function ltrim(string $key, int $start, int $end)
    {
        return $this->doReturn($this->redis->ltrim($key, $start, $end));
    }

    public function rPush(string $key, ...$elements)
    {
        return $this->doReturn($this->redis->rPush($key, ...$elements));
    }

    public function lrange(string $key, int $start, int $end)
    {
        return $this->doReturn($this->redis->lrange($key, $start, $end));
    }

    /**
     * @return Self|int|false
     */
    public function lPush($key, ...$elements)
    {
        return $this->doReturn($this->redis->lPush($key, ...$elements));
    }

    /**
     * @param array|string $key_or_keys
     * @param string|float|int $timeout_or_key
     */
    public function blPop($key_or_keys, $timeout_or_key, ...$extra_args)
    {
        return $this->doReturn($this->redis->blPop($key_or_keys, $timeout_or_key, ...$extra_args));
    }

    public function lPos(string $key, $value)
    {
        if ($this->capacity->has(RedisFunction::LPOS)) {
            $res = null;
            if (method_exists($this->redis, "lPos")) {
                $res = $this->redis->lPos($key, $value);
            } else {
                $res = $this->redis->rawcommand("LPOS", $key, $value);
            }
            return $this->doReturn($res);
        }

        if (!$this->isBase) {
            throw new UnsupportedRedis();
        }
        $pos = 0;
        while (true) {
            $keys = $this->redis->lrange($key, $pos, $pos + 99);
            if (empty($keys) || !is_array($keys)) {
                return false;
            }
            $found = array_search($value, $keys, true);
            if ($found !== false) {
                return $pos + $found;
            }
            if (count($keys) < 100) {
                return false;
            }
            $pos += 100;
        }
    }

    public function copyReplace(string $src, string $dst)
    {
        if ($this->capacity->has(RedisFunction::COPY)) {
            $res = null;
            if (method_exists($this->redis, "copy")) {
                $res = $this->redis->copy($src, $dst, ["replace"]);
            } else {
                $res = $this->redis->rawcommand("COPY", $src, $dst, "replace");
            }
            return $this->doReturn($res);
        }

        $this->unlink($dst);
        $pos = 0;
        // need two connections to read during transaction
        if ($this->isBase) {
            $base = $this->redis;
        } else if ($this->base == null) {
            $factory = new RedisFactory(
                $this->context->get(SystemConfig::class),
                $this->context->get(IEventLogger::class)
            );
            $base = $this->base = $factory->getInstance();
        } else {
            $base = $this->base;
        }
        while (true) {
            $keys = $base->lrange($src, $pos, $pos + 99);
            if (empty($keys) || !is_array($keys)) {
                break 1;
            }
            $this->redis->rpush($dst, ...$keys);
            if (count($keys) < 100) {
                break 1;
            }
            $pos += 100;
        }
    }
}
