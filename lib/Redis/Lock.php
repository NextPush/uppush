<?php
declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Redis;

use OCA\UnifiedPushProvider\Exceptions\AbortedException;
use OCA\UnifiedPushProvider\Exceptions\LockException;

use OCA\UnifiedPushProvider\Redis\RedisHelper;


/**
 * Lock to prevent multiple php process to run at the same time.
 *
 * This is used to prevent 2 processes to add messages in the lists at the same time, to ensure
 * the message lists and the popped lists have the same order.
 *
 * Lock is automaticaly released after a timeout, or in destructor (called even if exit is used)
 */
final class Lock {
	/**
	 * @var string key to lock
	 */
	private string $key;

	/**
	 * @var bool true if lock is set
	 */
	private bool $locked;

	/**
	 * @var int timeout in seconds, before lock is automaticaly released
	 */
	private const timeoutSec = 10;

	/** @var RedisHelper */
	private RedisHelper $redis;

	/** @var string */
	private string $token;

	/**
	 * Acquire lock
	 * @param RedisHelper $redis
	 * @param string $deviceId
	 * @param string $token
	 */
	function __construct(
		RedisHelper $redis,
		string $deviceId,
		string $token
	) {
		$this->redis = $redis;
		$this->token = $token;
		$this->key = "uppush.$deviceId.lock";
		$this->locked = false;

		$this->lock();
	}

	public function watch(): void {
		$this->redis->watch($this->key);
		if ($this->redis->get($this->key) !== $this->token) {
			throw new LockException();
		}
	}

	/**
	 * Destructor, unlock if locked
	 */
	function __destruct() {
		$this->unlock();
	}

	/**
	 * Try to lock.
	 * If already locked, refresh it.
	 * If another process has locked, wait and try again, with a timeout around 5 seconds
	 *
	 * @throws LockException if timeout is reached, or if refresh is not possible
	 * @throws AbortedException if connection is aborted
	 */
	private function lock(): void {
		$n = time();
		// acquire lock. Try for 15 seconds
		$end = $n + 15;
		while ($n < $end) {
			// create a key only if no exists, will automaticaly unlock after `timeoutSec` seconds
			if ($this->redis->set($this->key, $this->token, ['nx', 'ex' => self::timeoutSec])) {
				$this->locked = true;
				return;
			}
			usleep(100 * 1000 + rand(0, 50000));
			if (connection_status() != 0) {
				throw new AbortedException();
			}
			$n = time();
		}
		throw new LockException();
	}

	/**
	 * Unlock
	 */
	public function unlock(): void {
		if ($this->locked) {
			$this->redis->watch($this->key);
			if ($this->redis->get($this->key) === $this->token) {
				$this->redis->multi()
					->unlink($this->key)
					->exec();
			}
			$this->redis->unwatch();
			$this->locked = false;
		}
	}
}
