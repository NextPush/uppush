<?php
declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Redis;

class RedisFunction
{
    public const LPOS = 0;
    public const UNLINK = 1;
    public const COPY = 2;
    public const WATCH = 3;
    public const MULTI = 4;
    public const BLPOP = 5;

    /**
     * @return array
     */
    public static function cases(): array {
        return array("LPOS", "UNLINK", "COPY", "WATCH", "MULTI", "BLPOP");
    }
}