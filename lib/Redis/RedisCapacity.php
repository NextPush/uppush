<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Redis;

use OCA\UnifiedPushProvider\Redis\RedisFunction;

use Redis;
use Throwable;

class RedisCapacity
{
    /** @var int */
    private int $capacity;

    /**
     * @param Redis $redis
     */
    public function __construct(
        Redis $redis
    ) {
        $capacity = 0;
        $functions = RedisFunction::cases();
        try {
            $infos = $redis->command("info", ...$functions);
        } catch(Throwable $e) {
            $multi = $redis->pipeline();
            foreach ($functions as $key => $function) {
                $multi->command("info", $function);
            }
            $infos = $multi->exec();
        }

        if (!empty($infos)) {
            foreach ($infos as $key => $info) {
                if ($info !== false && !(is_array($info) && $info[0] === false)) {
                    $capacity += 1 << $key;
                }
            }
        }
        $this->capacity = $capacity;
    }

    /**
     * @param int $function
     * @return bool
     */
    public function has(int $function): bool
    {
        return ($this->capacity & (1 << $function)) !== 0;
    }
}
