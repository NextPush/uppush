<?php
declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Controller;

use OCA\UnifiedPushProvider\Redis\RedisHelper;
use OCA\UnifiedPushProvider\Device\Device;
use OCA\UnifiedPushProvider\Device\Event;
use OCA\UnifiedPushProvider\Device\EventType;
use OCA\UnifiedPushProvider\Device\PushError;
use OCA\UnifiedPushProvider\Device\Urgency;
use OCA\UnifiedPushProvider\Device\SubscriptionInfo;
use OCA\UnifiedPushProvider\Responses\SyncDeviceResponse;
use OCA\UnifiedPushProvider\Request\RequestPush;
use OCA\UnifiedPushProvider\Request\RequestSync;
use OCA\UnifiedPushProvider\Db\UppushApp;
use OCA\UnifiedPushProvider\Db\UppushConfig;
use OCA\UnifiedPushProvider\Db\UppushDevice;

use OCP\AppFramework\ApiController;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\JSONResponse;
use OCP\IRequest;
use Psr\Container\ContainerInterface;
use Exception;

class UnifiedPushProviderController extends ApiController {
	/** @var ContainerInterface */
	private ContainerInterface $context;
	/** @var UppushConfig */
	private UppushConfig $config;
	/** @var UppushDevice */
	private UppushDevice $udevice;
	/** @var UppushApp */
	private UppushApp $uapp;

	/**
	 * @var int Max TTL for other events, 3 days
	 */
	private const maxTtlSec = 259200; // 3 * 24 * 60 * 60

	public function __construct(
		$appName,
		IRequest $request,
		ContainerInterface $context,
		UppushConfig $config,
		UppushDevice $udevice,
		UppushApp $uapp
	){
		$this->context = $context;
		$this->config = $config;
		$this->udevice = $udevice;
		$this->uapp = $uapp;
        parent::__construct(
            $appName,
            $request,
            'POST',
            'Authorization, Content-Encoding, TTL',
            1728000
        );
	}

	/**
	 * Push a message to a device
	 *
	 * @param string $token the application token
	 * @param string $message the content of the push message
	 * @param RequestPush $pushConfig
	 * @param ?RedisHelper $redis
	 *
	 * @return string|int the message id on success, or an error
	 */
	private function _push(
		string $token,
		string $messageStr,
		RequestPush $pushConfig,
		?RedisHelper $redis = null
	) {
		$subscriptionInfo = $this->uapp->getInfos($token);
		if ($subscriptionInfo === null) return PushError::NoDevice;

		$validate = $subscriptionInfo->validate($pushConfig->authorization);
		if($validate !== null) {
			return $validate;
		}

		$message = new Event(
			EventType::Message,
			$token,
			base64_encode($messageStr)
		);

		try {
			return Device::withDevice(
				$this->context, 
				$subscriptionInfo->deviceId, 
				function(Device $device) use ($message, $pushConfig) {
					// TODO manage 0 seconds ttl
					return $device->push(
						$message, 
						max($pushConfig->ttl, 10), 
						$pushConfig->urgency, 
						$pushConfig->topic,
					);
				},
				$redis,
			);
		} catch(Exception $e) {
			return PushError::Unknown;
		}
	}

	/**
	 *
	 * @NoCSRFRequired
	 * @NoAdminRequired
	 *
	 * @return JSONResponse
	 */
	public function check(): JSONResponse {
		return new JSONResponse(['success' => true]);
	}

	/**
	 * Set keepalive interval.
	 *
	 * @NoCSRFRequired
	 *
	 * @param int $keepalive
	 *
	 * @return JSONResponse
	 */
	public function setKeepalive(int $keepalive): JSONResponse{
		if(!$this->config->setKeepalive($keepalive)) {
			return new JSONResponse(['success' => false], Http::STATUS_BAD_REQUEST);
		}
		return new JSONResponse(['success' => true]);
	}

	/**
	 * Set max message ttl
	 *
	 * @NoCSRFRequired
	 *
	 * @param int $max_message_ttl the max message ttl
	 *
	 * @return JSONResponse
	 */
	public function setMaxMessageTtl(int $max_message_ttl): JSONResponse{
		if(!$this->config->setMaxMessageTtl($max_message_ttl)) {
			return new JSONResponse(['success' => false], Http::STATUS_BAD_REQUEST);
		}
		return new JSONResponse(['success' => true]);
	}

	/**
	 * Request to create a new deviceId.
	 *
	 * @CORS
	 * @NoCSRFRequired
	 * @NoAdminRequired
	 *
	 * @param string $deviceName
	 *
	 * @return JSONResponse
	 */
	public function createDevice(string $deviceName): JSONResponse {
		$deviceId = $this->udevice->create($deviceName);
		
		if($deviceId == null) {
			return new JSONResponse([
				'success' => false,
			]);
		} else {
			return new JSONResponse([
				'success' => true,
				'deviceId' => $deviceId
			]);
		}
	}

	/**
	 * Request to get push messages.
	 * This is a public page since it has to be handle by the non-connected app
	 * (NextPush app and not Nextcloud-app)
	 *
	 * @NoCSRFRequired
	 * @PublicPage
	 *
	 * @param string $deviceId
	 */
	public function syncDevice(string $deviceId) {
		if (!$this->udevice->exists($deviceId)) return new JSONResponse(['success' => false], Http::STATUS_UNAUTHORIZED);

		/** @var RequestSync */
		$syncConfig = $this->context->get(RequestSync::class);

		return new SyncDeviceResponse(
			$this->context,
			$syncConfig,
			$deviceId,
		);
	}

	/**
	 * Delete a device.
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @param string $deviceId
	 *
	 * @return JSONResponse
	 */
	public function deleteDevice(
		string $deviceId
	): JSONResponse {
		if($this->udevice->delete($deviceId)) {
			try {
				Device::withDevice($this->context, $deviceId, function(Device $device) {
					$device->erase();
				});
			} catch(Exception $e) {
			}
		}
		return new JSONResponse(['success' => true]);
	}

	/**
	 * Create an authorization token for a new 3rd party service.
	 *
	 * @NoAdminRequired
	 * @CORS
	 * @NoCSRFRequired
	 *
	 * @param string $deviceId
	 * @param string $appName
	 * @param string $vapid
	 *
	 * @return JSONResponse
	 */
	public function createApp(
		string $deviceId,
		string $appName,
		?string $vapid
	): JSONResponse {
		if (!$this->udevice->exists($deviceId)) return new JSONResponse(['success' => false], Http::STATUS_UNAUTHORIZED);
		if($vapid != null && !SubscriptionInfo::checkVapidPubKey($vapid)) {
			return new JSONResponse(['success' => false], Http::STATUS_BAD_REQUEST);
		}

		$token = $this->uapp->create($deviceId, $appName, $vapid);
		if($token == null) {
			return new JSONResponse([
				'success' => false,
			]);
		} else {
			return new JSONResponse([
				'success' => true,
				'token' => $token,
			]);
		}
	}

	/**
	 * Delete an authorization token.
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @param string $token
	 *
	 * @return JSONResponse
	 */
	public function deleteApp(string $token){
		$subscriptionInfo = $this->uapp->getInfos($token);
		if ($subscriptionInfo === null) return new JSONResponse(['success' => true]);

		if($this->uapp->delete($token)) {
			try {
				$message = new Event(
					EventType::DeleteApp,
					$token,
					null
				);
				Device::withDevice($this->context, $subscriptionInfo->deviceId, function(Device $device) use ($message) {
					$device->push($message, Self::maxTtlSec, Urgency::High, null);
				});
			} catch(Exception $e) {
			}
			return new JSONResponse(['success' => true]);
		}

	}

	/**
	 * Receive notifications from 3rd parties.
	 *
	 * @PublicPage
	 * @CORS
	 * @NoCSRFRequired
	 *
	 * @param string $token
	 *
	 * @return JSONResponse
	 */
	public function push(string $token): JSONResponse {
		$message = file_get_contents('php://input');
		/** @var RequestPush */
		$pushConfig = $this->context->get(RequestPush::class);
		$messageId = $this->_push($token, $message, $pushConfig);
		if($messageId === PushError::NoDevice) {
			return new JSONResponse(['success' => false], Http::STATUS_NOT_FOUND);
		} else if($messageId === PushError::AuthorizationRequired) {
			return new JSONResponse(['success' => false], Http::STATUS_UNAUTHORIZED);
		} else if($messageId === PushError::AuthorizationFailed) {
			return new JSONResponse(['success' => false], Http::STATUS_FORBIDDEN);
		} else if(empty($messageId) || !is_string($messageId)) {
			return new JSONResponse(['success' => false], Http::STATUS_INTERNAL_SERVER_ERROR);
		}
		$response = new JSONResponse(['success' => true], Http::STATUS_CREATED);
		$response->setHeaders(array('TTL' => $pushConfig->ttl, 'Location' => "../message/$messageId"));
		return $response;
	}

	/**
	 * Delete a notification
	 *
	 * @PublicPage
	 * @CORS
	 * @NoCSRFRequired
	 *
	 * @param string $messageId message id
	 *
	 * @return JSONResponse
	 */
	public function deleteMessage(string $messageId): JSONResponse {
		RedisHelper::withHelper($this->context, function(RedisHelper $redis) use ($messageId) {
			$messages = $redis->keys("uppush.*.message.$messageId");
			if(!empty($messages)) {
				$redis->unlink($messages);
			}
		});
		return new JSONResponse(['success' => true]);
	}

	/**
	 * Unifiedpush discovery
	 * Following specifications
	 *
	 * @CORS
	 * @PublicPage
	 * @NoCSRFRequired
	 *
	 * @return JSONResponse
	 */
	public function unifiedpushDiscovery(): JSONResponse {
		return new JSONResponse([
				'unifiedpush' => [
					'version' => 1
				]
			]);
	}

	/**
	 * GATEWAYS
	 */

	/**
	 * Matrix Gateway discovery
	 *
	 * @CORS
	 * @PublicPage
	 * @NoCSRFRequired
	 *
	 * @return JSONResponse
	 */
	public function gatewayMatrixDiscovery(): JSONResponse {
		return new JSONResponse([
				'unifiedpush' => [
					'gateway' => 'matrix'
				]
			]);
	}

	/**
	 * Matrix Gateway
	 *
	 * @CORS
	 * @PublicPage
	 * @NoCSRFRequired
	 *
	 * @return JSONResponse
	 */
	public function gatewayMatrix(): JSONResponse {
		$message = json_decode(file_get_contents('php://input'));
		$rejected = [];
		$devices = $message->notification->devices;
		unset($message->notification->devices);
		/** @var RequestPush */
		$pushConfig = $this->context->get(RequestPush::class);
		return RedisHelper::withHelper($this->context, function(RedisHelper $redis) use ($devices, $message, $pushConfig, &$rejected) {
			foreach ($devices as $device) {
				$pushkey = $device->pushkey;
				$exploded_pushkey = explode('/', $pushkey);
				$token = end($exploded_pushkey);
				$id = $this->_push($token, json_encode($message), $pushConfig, $redis);
				if($id === PushError::NoDevice) {
					array_push($rejected, $pushkey);
				}
			}
			return new JSONResponse([
				'rejected' => $rejected
			]);
		});
	}
}
