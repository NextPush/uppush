<?php

namespace OCA\UnifiedPushProvider\Cron;

use OC\User\Manager;
use OCA\UnifiedPushProvider\Db\UppushDevice;
use OCA\UnifiedPushProvider\Redis\RedisHelper;
use OCP\BackgroundJob\TimedJob;
use OCP\AppFramework\Utility\ITimeFactory;
use OCP\BackgroundJob\IJob;
use Psr\Container\ContainerInterface;

class CheckDatas extends TimedJob
{
    /** @var Manager */
    private ContainerInterface $context;

    public function __construct(ITimeFactory $time, ContainerInterface $context)
    {
        parent::__construct($time);

        $this->context = $context;

        if (method_exists($this, 'setTimeSensitivity')) {
            $this->setTimeSensitivity(IJob::TIME_INSENSITIVE);
        }
        if (method_exists($this, "setAllowParallelRuns")) {
            $this->setAllowParallelRuns(false);
        }

        // Run once per day
        $this->setInterval(60 * 60 * 24);
    }

    /**
     * @param Manager $userManager
     * @param UppushDevice $udevice
     */
    private static function deleteOldUsers(Manager $userManager, UppushDevice $udevice)
    {
        $users = $udevice->getAllUsers();
        foreach ($users as $user) {
            if (!$userManager->userExists($user)) {
                $udevice->deleteUser($user);
            }
        }
    }

    /**
     * @param RedisHelper $redis
     * @param UppushDevice $udevice
     */
    private static function deleteOldRedisKeys(RedisHelper $redis, UppushDevice $udevice)
    {
        $keys = $redis->keys("uppush.*");
        $toDelete = array();
        $devices = $udevice->getAlldevices();
        $device_map = array();
        foreach ($devices as $device) {
            $device_map[$device] = true;
        }
        foreach ($keys as $key) {
            $sub = explode('.', $key, 3);
            if (count($sub) < 2 || ($sub[1] != "version" && !isset($device_map[$sub[1]]))) {
                array_push($toDelete, $key);
            }
        }
        if (!empty($toDelete)) {
            $redis->unlink($toDelete);
        }
    }

    protected function run($arguments)
    {
        /** @var UppushDevice */
        $udevice = $this->context->get(UppushDevice::class);

        // first check users
        /** @var Manager */
        $userManager = $this->context->get(Manager::class);
        Self::deleteOldUsers($userManager, $udevice);

        // then check redis keys
        /** @var RedisHelper */
        $redis = $this->context->get(RedisHelper::class);
        try {
            Self::deleteOldRedisKeys($redis, $udevice);
        } finally {
            $redis->close();
        }
    }
}
