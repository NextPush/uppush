<?php
declare(strict_types=1);
namespace OCA\UnifiedPushProvider\Exceptions;

use Exception;

final class UnsupportedRedis extends Exception {}