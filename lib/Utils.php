<?php

namespace OCA\UnifiedPushProvider;

use OC\RedisFactory;
use OCA\UnifiedPushProvider\Exceptions\UnsupportedRedis;
use OCA\UnifiedPushProvider\Redis\RedisHelper;
use Psr\Container\ContainerInterface;

class Utils {
	public const DB_IDS_LENGTH = 36;

	/**
	 * Generates a random UUID
	 * @return string
	 */
	public static function uuid(): string {
		return str_replace('=', '', strtr(base64_encode(random_bytes(20)), '+/', '-_'));
	}

	public static function getRedisError(
		ContainerInterface $context
	): ?string {
		$redisFactory = $context->get(RedisFactory::class);
		if (!$redisFactory->isAvailable()) {
			return "Redis support is not available";
		}
		try {
			/** @var RedisHelper */
			$context->get(RedisHelper::class);
		} catch (UnsupportedRedis $e) {
			return "This version of redis is not supported";
		} catch (\Exception $e) {
			return $e->getMessage();
		}
		return null;
	}
}
