<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\AppInfo;

use OCP\App\IAppManager;
use OCP\Capabilities\ICapability;
use OCA\UnifiedPushProvider\Utils;
use Psr\Container\ContainerInterface;

class Capabilities implements ICapability {
	/** @var IAppManager */
	private IAppManager $appManager;
	/** @var ContainerInterface */
	private ContainerInterface $context;
	
	/**
	 * @param IAppManager $appManager
	 * @param ContainerInterface $context
	 */
	public function __construct(
		IAppManager $appManager,
		ContainerInterface $context
	) {
			$this->appManager = $appManager;
			$this->context = $context;
	}

	public function getCapabilities(): array {
		return [
			Application::APP_ID => [
					"redis" => Utils::getRedisError($this->context) === null,
					"version" => $this->appManager->getAppVersion(Application::APP_ID)
				],
		];
	}
}
