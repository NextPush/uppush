<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\AppInfo;

use OCA\UnifiedPushProvider\Redis\RedisHelper;

use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use Psr\Container\ContainerInterface;

class Application extends App implements IBootstrap {
    public const APP_ID = 'uppush';

    public function __construct() {
        parent::__construct(self::APP_ID);
    }

    /**
     * @param IRegistrationContext $context
     */
    public function register(IRegistrationContext $context): void {
        $context->registerCapability(Capabilities::class);

        $context->registerService(RedisHelper::class, function(ContainerInterface $context) {
            return RedisHelper::create($context);
        });
    }

    /**
     * @param IBootContext $context
     */
    public function boot(IBootContext $context): void {}
}
