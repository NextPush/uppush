<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Device;

use OCA\UnifiedPushProvider\Device\PushError;
use OCA\UnifiedPushProvider\Exceptions\InvalidVapidKey;
use OCA\UnifiedPushProvider\Request\RequestAuthorization;

use OCP\IDBConnection;
use OC\Security\TrustedDomainHelper;

use Psr\Container\ContainerInterface;
use Throwable;

final class SubscriptionInfo
{
    private static function maybeLoadFirebase()
    {
        // maybe load 3rd party firebase/jwt
        foreach([
            "JWTExceptionWithPayloadInterface",
            "BeforeValidException",
            "CachedKeySet",
            "ExpiredException",
            "JWK",
            "JWT",
            "Key",
            "SignatureInvalidException",
        ] as $cl) {
            if (!class_exists("\\UPPUSH\\Firebase\\JWT\\$cl")) {
                require_once __DIR__ . "/../../3rdparty/jwt/$cl.php";
            }
        }
    }

    /**
     * @param string $pubKey a VAPID public key
     * @return ?string the raw public key, or null if the key is invalid
     */
    private static function getVapidPubKeyRaw(string $pubKey): ?string
    {
        $k = \UPPUSH\Firebase\JWT\JWT::urlsafeB64Decode($pubKey);
        if (strlen($k) != 65) {
            return null;
        }
        $type = ord($k[0]);
        // only accept uncompressed form of x9.62: rfc8292 3.2
        if ($type !== 0x04) {
            return null;
        }
        return $k;
    }


    /**
     * @param string $pubKey a VAPID public key
     * @return bool true if the key is valid, false otherwise
     */
    public static function checkVapidPubKey(string $pubKey): bool
    {
        Self::maybeLoadFirebase();
        return Self::getVapidPubKeyRaw($pubKey) !== null;
    }

    /** @var ContainerInterface */
    private ContainerInterface $context;
    /** @var IDBConnection */
    private IDBConnection $db;
    /** @var string */
    public string $token;
    /** @var string */
    public string $deviceId;
    /** @var ?string */
    public ?string $vapidPubkey;
    /** @var ?string */
    private ?string $vapidLastJWT;
    /** @var int */
    private int $vapidLastJWTExpire;

    function __construct(
        ContainerInterface $context,
        IDBConnection $db,
        string $token,
        string $deviceId,
        ?string $vapidPubkey,
        ?string $vapidLastJWT,
        int $vapidLastJWTExpire = 0
    ) {
        $this->context = $context;
        $this->db = $db;
        $this->token = $token;
        $this->deviceId = $deviceId;
        $this->vapidPubkey = $vapidPubkey;
        $this->vapidLastJWT = $vapidLastJWT;
        $this->vapidLastJWTExpire = $vapidLastJWTExpire;
    }



    /**
     * @return Key a parsed version of the VAPID public key
     * @throws InvalidVapidKey if the key is invalid
     */
    private function parsePubKey(): \UPPUSH\Firebase\JWT\Key
    {
        $k = Self::getVapidPubKeyRaw($this->vapidPubkey);
        if ($k === null) {
            throw new InvalidVapidKey();
        }

        $x = base64_encode(substr($k, 1, 32));
        $y = base64_encode(substr($k, 33));

        return @\UPPUSH\Firebase\JWT\JWK::parseKey(["x" => $x,  "y" => $y, "kty" => "EC", "crv" => "P-256", "alg" => "ES256"]);
    }


    /**
     * @param ?string $t the JWT to cache, or null to invalidate
     * @param int $exp the expiration time of the JWT in seconds
     * @return void
     */
    private function updateDbCache(?string $t, int $exp): void
    {
        try {
            $query = $this->db->getQueryBuilder();
            $query->update("uppush_applications")
                ->set('vapid_jwt_cache', $query->createNamedParameter($t))
                ->set('vapid_jwt_cache_expire', $query->createNamedParameter($exp))
                ->where($query->expr()->eq('token', $query->createNamedParameter($this->token)));
            $r = $query->execute();
        } catch (Throwable $ex) {
            // ignore
        }
    }

    /**
     * @param string $url
     * @return bool
     */
    private function isTrustedUrl(string $url): bool
    {
        $trustedDomainHelper = $this->context->get(TrustedDomainHelper::class);
        if(method_exists($trustedDomainHelper, "isTrustedUrl")) {
            return $trustedDomainHelper->isTrustedUrl($url);
        }
        $parsedUrl = parse_url($url);
        if (empty($parsedUrl['host'])) {
            return false;
        }

        if (isset($parsedUrl['port']) && $parsedUrl['port']) {
            return $trustedDomainHelper->isTrustedDomain($parsedUrl['host'] . ':' . $parsedUrl['port']);
        }

        return $trustedDomainHelper->isTrustedDomain($parsedUrl['host']);
    }

    /**
     * @param ?string $authorization
     * @return ?int null if the subscription is valid, the error otherwise
     */
    public function validate(?string $authorization): ?int
    {
        if ($this->vapidPubkey == null) {
            return null;
        }

        if (empty($authorization)) {
            return PushError::AuthorizationRequired;
        }

        if (!str_starts_with($authorization, "vapid ")) {
            return PushError::AuthorizationFailed;
        }

        $authorization = explode(',', trim(substr($authorization, 6)));

        $k = null;
        $t = null;
        foreach ($authorization as $keyValue) {
            $exploded = explode('=', trim($keyValue));
            if (count($exploded) !== 2) {
                continue;
            }
            $key = trim($exploded[0]);
            if ($key === "k") {
                $k = trim($exploded[1]);
            } else if ($key === "t") {
                $t = trim($exploded[1]);
            }
            if ($t !== null && $k !== null) {
                break;
            }
        }

        if ($k !== $this->vapidPubkey || empty($t)) {
            return PushError::AuthorizationFailed;
        }

        Self::maybeLoadFirebase();

        if ($t === $this->vapidLastJWT) {
            // use cache
            $timestamp = is_null(\UPPUSH\Firebase\JWT\JWT::$timestamp) ? time() : \UPPUSH\Firebase\JWT\JWT::$timestamp;
            if (($timestamp - \UPPUSH\Firebase\JWT\JWT::$leeway) < $this->vapidLastJWTExpire) {
                return null;
            }
            // expired
            $this->updateDbCache(null, 0);
            return PushError::AuthorizationFailed;
        }

        $jwt = null;
        try {
            $k = $this->parsePubKey();
            $jwt = \UPPUSH\Firebase\JWT\JWT::decode(
                $t,
                $k,
            );
            if (
                !isset($jwt->exp)
                || !is_int($jwt->exp)
                || !isset($jwt->aud)
                || !is_string($jwt->aud)
                || empty($jwt->aud)
                || !$this->isTrustedUrl($jwt->aud)
            ) {
                return PushError::AuthorizationFailed;
            }
        } catch (Throwable $ex) {
            return PushError::AuthorizationFailed;
        }

        $this->updateDbCache($t, $jwt->exp);
        return null;
    }
}
