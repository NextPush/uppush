<?php

declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Device;

use OCA\UnifiedPushProvider\Utils;
use OCA\UnifiedPushProvider\Redis\RedisHelper;
use OCA\UnifiedPushProvider\Redis\Lock;
use OCA\UnifiedPushProvider\Device\Event;
use OCA\UnifiedPushProvider\Device\Urgency;
use OCA\UnifiedPushProvider\Device\IdByUrgency;
use OCA\UnifiedPushProvider\Exceptions\AbortedException;
use OCA\UnifiedPushProvider\Exceptions\LockException;

use Redis;
use Exception;
use OC_Util;
use OCA\UnifiedPushProvider\Request\RequestSync;
use Psr\Container\ContainerInterface;

/**
 * Class to manage a device
 */
final class Device {
	/** @var RedisHelper */
	private RedisHelper $redis;
	/** @var string */
	private string $deviceId;

	/**
	 * @param ContainerInterface $context
	 * @param string $deviceId
	 * @param callable(Device):mixed $callback
	 * @param ?RedisHelper $redis
	 * @return mixed
	 */
	public static function withDevice(
		ContainerInterface $context,
		string $deviceId,
		callable $callback,
		?RedisHelper $redis = null
	) {
		$cb = function (RedisHelper $redis) use ($deviceId, $callback) {
			$device = new Device($redis, $deviceId);
			return $callback($device);
		};
		if ($redis == null) {
			return RedisHelper::withHelper($context, $cb);
		} else {
			return $cb($redis);
		}
	}

	/**
	 * @param RedisHelper $redis
	 * @param string $deviceId
	 */
	public function __construct(
		RedisHelper $redis,
		string $deviceId
	) {
		$this->redis = $redis;
		$this->deviceId = $deviceId;
	}

	/**
	 * The key used to set the sync owner
	 * uppush.$deviceId.owner
	 *
	 * @return string
	 */
	private function getOwnerKey(): string {
		return "uppush.$this->deviceId.owner";
	}

	/**
	 * The key used to set the last sent message id
	 * uppush.$deviceId.last_sent_id.$urgency
	 *
	 * This value is used when the LastEventId http header is not used
	 * @param int $urgency
	 * @return string
	 */
	private function getLastSentIdKey(int $urgency): string {
		return "uppush.$this->deviceId.last_sent_id." . Urgency::toString($urgency);
	}

	/**
	 * The key used to set the list of available messages
	 * uppush.$deviceId.messages.$urgency
	 *
	 * @param int $urgency
	 * @return string
	 */
	private function getMessageListKey(int $urgency): string {
		return "uppush.$this->deviceId.messages." . Urgency::toString($urgency);
	}

	/**
	 * The key used to set the list of available messages
	 * uppush.$deviceId.popped_messages.$urgency
	 *
	 * @param int $urgency
	 * @return string
	 */
	private function getPoppedMessageListKey(int $urgency): string {
		return "uppush.$this->deviceId.popped_messages." . Urgency::toString($urgency);
	}

	/**
	 * The base key used for messages
	 * uppush.$this->deviceId.message.
	 *
	 * @return string
	 */
	private function getMessageBaseKey(): string {
		return "uppush.$this->deviceId.message.";
	}

	/**
	 * The key used to set the message id for a topic
	 * uppush.$deviceId.topic.$topic
	 *
	 * This key is used when the Topic http header is used
	 * @return string
	 */
	private function getTopicKey(string $topic): string {
		return "uppush.$this->deviceId.topic.$topic";
	}

	/**
	 * Delete all data of the device
	 * @return void
	 */
	public function erase(): void {
		$key = "uppush.$this->deviceId.*";
		$exists = $this->redis->keys($key);
		if(empty($exists)) {
			return;
		}
		// erase all keys of the device
		$this->redis->unlink($exists);
		// notify existing listeners
		$this->redis->rPush($this->getPoppedMessageListKey(Urgency::High), "close");
		// wait for notification to be received
		usleep(200 * 1000);

		// check again keys exiting keys. It may contains previous setted notification key.
		$exists = $this->redis->keys($key);
		if(!empty($exists)) {
			$this->redis->unlink($exists);
		}
	}

	/**
	 * Add a new message to the device. If topic is given, override pending messages
	 * with the same topic.
	 *
	 * @param Event $event
	 * @param int $ttlSec number of seconds before removing (not sent) push message
	 * @param int $urgency
	 * @param string $topic optional topic
	 *
	 * @throws Exception
	 *
	 * @return string the message id
	 */
	public function push(Event $event, int $ttlSec, int $urgency, ?string $topic): string {
		ignore_user_abort(true);
		$messageBase = $this->getMessageBaseKey();

		$topicKey = null;
		$oldMessageIdForTopic = null;

		// calculate new id
		$messageId = Utils::uuid();

		// We lock to ensure messages in message list and popped message list
		// are in the same order
		$lock = new Lock($this->redis, $this->deviceId, $messageId);
		try {
			// variables to get
			if (!empty($topic)) {
				$topicKey = $this->getTopicKey($topic);
				$oldMessageIdForTopic = $this->redis->get($topicKey);
			}

			// start transaction
			$lock->watch();
			$multi = $this->redis->multi();

			// set the message
			$multi->set($messageBase . $messageId, json_encode($event), ['ex' => $ttlSec]);

			// check if topic is setted
			if (!empty($topic)) {
				// set new messageId for this topic
				$multi->set($topicKey, $messageId, ['ex' => $ttlSec]);

				if (!empty($oldMessageIdForTopic)) {
					// delete old topic push message
					$multi->unlink($messageBase . $oldMessageIdForTopic);
				}
			}

			$multi
				->rPush($this->getMessageListKey($urgency), $messageId)
				->rPush($this->getPoppedMessageListKey($urgency), $messageId);

			// execute transaction
			if ($multi->exec() === false) {
				throw new Exception("can not set message");
			}
			$this->redis->unwatch();
			return $messageId;
		} finally {
			$lock->unlock();
		}
	}

	/**
	 * Check if the owner of the device is still the given one.
	 * Add a redis watch arround owner
	 * @param string $owner the expected owner
	 * @throws AbortedException if the owner is not the expected one
	 * @return void
	 */
	private function checkIsOwner(string $owner): void {
		$ownerKey = $this->getOwnerKey();
		$this->redis->watch($ownerKey);
		if ($this->redis->get($ownerKey) !== $owner) {
			throw new AbortedException();
		}
	}

	/**
	 * Check if the connection status is normal.
	 * Throw an exception if the connection was closed.
	 * @throws AbortedException if the connection was closed.
	 * @return void
	 */
	private static function checkConnectionStatus(): void {
		if (connection_status() != 0) {
			throw new AbortedException();
		}
	}

	/**
	 * Send an event to the client
	 * @param string $id the id of the event
	 * @param string $jsonEvent the json representation of the event
	 * @param int $urgency the urgency of the event
	 * @return void
	 */
	private function sendEvent(string $id, string $jsonEvent, int $urgency): void {
		// No need to decode jsonEvent to Event class, we
		// just take the type.
		$urgencyStr = Urgency::toString($urgency);
		echo 'event:' . json_decode($jsonEvent)->type . PHP_EOL;
		echo 'id:' . $urgencyStr . "." . $id . PHP_EOL;
		echo 'data:' . $jsonEvent . PHP_EOL;
		echo PHP_EOL;
		flush();

		Self::checkConnectionStatus();
		usleep(500 * 1000);
	}

	/**
	 * If the last event id was not given, read it from redis
	 * @param RequestSync $syncConfig
	 * @return void
	 */
	private function maybeReadLastEventId(RequestSync $syncConfig): void {
		$events = array();
		for($curUrgency = Urgency::High; $curUrgency >= $syncConfig->urgency; --$curUrgency) {
			$curLastEventId = $syncConfig->lastEventIds->get($curUrgency);
			if ($curLastEventId == null) {
				array_push($events, $this->getLastSentIdKey($curUrgency));
			}
		}
		if (!empty($events)) {
			$events = $this->redis->mget($events);
			$i = 0;
			for($curUrgency = Urgency::High; $curUrgency >= $syncConfig->urgency; --$curUrgency) {
				$curLastEventId = $syncConfig->lastEventIds->get($curUrgency);
				if ($curLastEventId == null) {
					$l = $events[$i++];
					if (!empty($l)) {
						$syncConfig->lastEventIds->set($curUrgency, $l);
					}
				}
			}
		}
	}

	/**
	 * Synchronize old messages with the client
	 *
	 * 1. We remove acknowleged messages
	 * 2. We replace the current popped lists with unacknowledged messages
	 *
	 * @param string $owner the owner of the device
	 * @param RequestSync $syncConfig
	 *
	 * @return void
	 */
	private function syncOldMessages(
		string $owner,
		RequestSync $syncConfig
	): void {
		$baseMessageKey = $this->getMessageBaseKey();

		$this->maybeReadLastEventId($syncConfig);

		// First, we remove received messages
		for($curUrgency = Urgency::High; $curUrgency >= $syncConfig->urgency; --$curUrgency) {
			$listKey = $this->getMessageListKey($curUrgency);
			$lastEventId = $syncConfig->lastEventIds->get($curUrgency);
			if($lastEventId == null) {
				continue;
			}
			$posLastEventId = $this->redis->lPos($listKey, $lastEventId);
			if (!is_int($posLastEventId)) {
				// The lastEventId isn't in the list
				continue;
			}

			// loop until reached $posLastEventId
			while ($posLastEventId > -1) {
				// get all ids until $posLastEventId included, but max 100
				$messageIds = $this->redis->lrange($listKey, 0, min(99, $posLastEventId));
				$this->checkIsOwner($owner);
				$multi = $this->redis->multi();
				// Position in the list can't change between lPos and lrange
				// because ids in this list never expire, and this is the only way
				// to remove them.
				// So, if lastEventId is in the messageIds, posLastEventId is its index
				foreach($messageIds as $messageId) {
					$multi->unlink($baseMessageKey . $messageId);
				}
				$got = count($messageIds);
				$multi->ltrim($listKey, $got , -1);
				if ($multi->exec() === false) {
					throw new AbortedException();
				}
				$this->redis->unwatch();
				// remove from posLastEventId what has been trimed
				$posLastEventId -= $got;
			}
		}

		// Then we replace the popped list with the current list of unread messages
		//
		// It needs a lock to ensure no message is posted during the copy
		{
			$lock = new Lock($this->redis, $this->deviceId, $owner);
			try {
				// start transaction
				$lock->watch();
				// start watch that we are still the owner
				$this->checkIsOwner($owner);
				$multi = $this->redis->multi();
				for($curUrgency = Urgency::High; $curUrgency >= $syncConfig->urgency; --$curUrgency) {
					$listKey = $this->getMessageListKey($curUrgency);
					$poppedKey = $this->getPoppedMessageListKey($curUrgency);
					// We get the messageId of the current last message recorded
					$multi->copyReplace($listKey, $poppedKey);
				}
				if ($multi->exec() === false) {
					throw new AbortedException();
				}
				$this->redis->unwatch();
			} finally {
				$lock->unlock();
			}
		}
	}

	/**
	 * Wait for new messages to be sent to the device.
	 * @param string $owner the owner of the device
	 * @param RequestSync $syncConfig
	 * @param int $lastSentData the last time data was sent
	 * @return void
	 */
	private function waitMessages(string $owner, RequestSync $syncConfig, int $lastSentData): void {
		// list of keys to listen
		$poppedKeys = array();
		for($curUrgency = Urgency::High; $curUrgency >= $syncConfig->urgency; --$curUrgency) {
			array_push($poppedKeys, $this->getPoppedMessageListKey($curUrgency));
		}
		$baseMessageKey = $this->getMessageBaseKey();

		while (true) {
			usleep(500 * 1000);
			$nextKeepalive = time() - $lastSentData + $syncConfig->keepalive;
			$notif = null;
			if ($nextKeepalive > 0) {
				$notif = $this->redis->blPop($poppedKeys, $nextKeepalive);
				Self::checkConnectionStatus();
			}
			if (empty($notif)) {
				$this->checkIsOwner($owner);
				$this->redis->unwatch();
				// ping
				echo 'event: ping' . PHP_EOL;
				echo 'data: {"type":"ping"}' . PHP_EOL;
				echo PHP_EOL;
				flush();
				Self::checkConnectionStatus();
				$lastSentData = time();
			} else if (!empty($notif[1])) {
				$messageId = $notif[1];
				$this->checkIsOwner($owner);
				if ($messageId == "close") {
					continue;
				}

				$key = $notif[0];
				$parts = explode(".", $key);
				$urgency = Urgency::fromString(end($parts), Urgency::Normal);

				$jsonEvent = $this->redis->get($baseMessageKey . $messageId);
				if (!empty($jsonEvent)) {
					$lastSentData = time();

					$this->sendEvent($messageId, $jsonEvent, $urgency);
				}
				$multi = $this->redis->multi();
				$multi->set($this->getLastSentIdKey($urgency), $messageId);
				if ($multi->exec() === false) {
					throw new AbortedException();
				}
				$this->redis->unwatch();
			}
		}
	}

	/**
	 * Start to sync event
	 * @param RequestSync $syncConfig
	 * @return void
	 */
	public function sync(RequestSync $syncConfig): void {
		$this->redis->redis->setOption(Redis::OPT_READ_TIMEOUT, -1);
		OC_Util::obEnd();

		echo 'event:start'.PHP_EOL;
		echo 'data:{"type":"start"}'.PHP_EOL;
		echo PHP_EOL;
		echo 'event:keepalive'.PHP_EOL;
		echo 'data:{"type":"keepalive","keepalive":'.$syncConfig->keepalive.'}'.PHP_EOL;
		echo PHP_EOL;
		flush();
		
		$lastSentData = time();

		// calculate an owner id
		$owner = Utils::uuid();

		set_time_limit(0);
		ignore_user_abort(true);

		try {
			// take ownership and notify old processes
			$this->redis->pipeline()
				->set($this->getOwnerKey(), $owner)
				->lPush($this->getPoppedMessageListKey(Urgency::High), "close")
				->exec();
			usleep(500 * 1000);

			$this->syncOldMessages($owner, $syncConfig);
			$this->waitMessages($owner, $syncConfig, $lastSentData);
		} catch (Exception $e) {
		} finally {
			sleep(1);
			echo 'event:close' . PHP_EOL;
			echo 'data:{"type":"close"}' . PHP_EOL;
			echo PHP_EOL;
			flush();
		}
	}
}
