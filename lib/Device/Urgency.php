<?php
declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Device;

/**
 * Enum for message urgencya
 */
class Urgency {
    public const VeryLow = 0;
    public const Low = 1;
    public const Normal = 2;
    public const High = 3;

    /**
     * Returns the string representation of the urgency
     * @return string
     */
    public static function toString(int $urgency): string {
        switch ($urgency) {
            case Urgency::VeryLow:
                return "very-low";
            case Urgency::Low:
                return "low";
            case Urgency::Normal:
                return "normal";
            case Urgency::High:
                return "high";
        }
    }

    /**
     * Creates an urgency from a string
     * @param string|null $str the string to parse
     * @return int
     */
    public static function fromString(?string $str): ?int {
        if (empty($str)) {
            return null;
        }
        switch (strtolower($str)) {
            case "very-low":
                return Urgency::VeryLow;
            case "low":
                return Urgency::Low;
            case "normal":
                return Urgency::Normal;
            case "high":
                return Urgency::High;
            default:
                return null;
        }
    }
}
