<?php
declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Device;

class PushError {
    const NoDevice = 1;
    const AuthorizationRequired = 2;
    const AuthorizationFailed = 3;
    const Unknown = 4;
    
}