<?php
declare(strict_types=1);

namespace OCA\UnifiedPushProvider\Device;

use OCA\UnifiedPushProvider\Device\Urgency;

final class IdByUrgency {
    /**
     * the ids by urgency
     */
    private array $ids;

    /**
     * User agent has to memorize each last received id by urgency.
     * All ids should to be sent using the Last-Event-Id header, as a comma separated list of ids for each urgency.
     * Example: high.abcdef,normal.ghijkl,low.mnopqr,very-low.stuvwx
     * The urgency prefixes seen above in the example are already added by server.
     * Examble of received event:
     *      event:message
     *      urgency:high
     *      id:high.abcdef
     *      data:...
     * 
     * @param string $id The Last-Event-ID http header
     */
    public function __construct(?string $header) {
        $this->ids = array_fill(0, 4, null);
        if (!empty($header)) {
            $headerParts = explode(",", $header, 4);
            foreach ($headerParts as $part) {
                $idPart = explode(".", trim($part));
                if (count($idPart) === 1) {
                    $this->ids[Urgency::Normal] = $idPart[0];
                } else {
                    $urgency = Urgency::fromString(trim($idPart[0]), Urgency::Normal);
                    $this->ids[$urgency] = $idPart[1];
                }
            }
        }
    }

    /**
     * Converts the ids array to a string or int representation. Int is used if all ids are the same
     *
     * @return string The string or int representation of ids
     */
    public function toKey(): string {
        $first = $this->ids[0];

        foreach ($this->ids as $id) {
            if ($id !== $first) {
                return implode('|', $this->ids);
            }
        }
        return $first;
    }

    /**
     * Returns the last sent id for the given urgency
     *
     * @param int $urgency The urgency to get the last sent id for
     * @return string The last sent id for the given urgency
     */
    public function get(int $urgency): ?string
    {
        return $this->ids[$urgency];
    }

    /**
     * Sets the last sent id for the given urgency
     *
     * @param int $urgency The urgency to set the last sent id for
     * @param string $id The new last sent id to set
     */
    public function set(int $urgency, string $id): void
    {
        $this->ids[$urgency] = $id;
    }
}
