<?php
declare(strict_types=1);
namespace OCA\UnifiedPushProvider\Device;

/**
 * Type of message that are sent through redis
 */
class EventType
{
	/*
	 * These events aren't passed via redis, and sent with string concatenation
	 *
	case Start = "start";
	case Ping = "ping";
	case Close = "close";
	// TODO: Keepalive should be updated when the setting is changed
	case Keepalive = "keepalive";
	 */

	public const Message = "message";
	public const DeleteApp = "deleteApp";
	public const Unknown = "unknown";

	static function from_value(string $value): string
	{
		switch ($value) {
			case self::Message:
			case self::DeleteApp:
				return $value;
			default:
				return self::Unknown;
		}
	}
}
