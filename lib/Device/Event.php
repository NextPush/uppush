<?php
declare(strict_types=1);
namespace OCA\UnifiedPushProvider\Device;

use OCA\UnifiedPushProvider\Device\EventType;

/**
 * Event object describing the type, the content and the recipient device
 */
final class Event implements \JsonSerializable
{
	/**
	 * @var string type of message
	 */
	public string $type;

	 /**
	  * @var string token of the recipient device
	  */
	public string $token;

	/**
	 * @var string content of the message
	 */
	public ?string $message;

	/*
	 * TODO: Keepalive should be updated when changed in the settings.
	 *
	 * @var int new keepalive value
	public ?int $keepalive;
	 */

	/**
	 * @param string $type
	 * @param string $token application token
	 * @param ?string $message content if type = Message
	 * @param ?int $keepalive if type = Keepalive
	 */
	function __construct(string $type, string $token, ?string $message)
	{
		$this->type = $type;
		$this->token = $token;
		$this->message  = $message;
	}

	/**
	 * @return mixed
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}

	static function from_json(string $json): Event
	{
		$dict = json_decode($json);
		return new Event(
			EventType::from_value($dict->type),
			$dict->deviceToken,
			$dict->content,
		);
	}
}

