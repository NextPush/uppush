<?php
namespace OCA\UnifiedPushProvider\Settings;

use OC\RedisFactory;
use OCA\UnifiedPushProvider\Db\UppushConfig;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\Settings\ISettings;
use OCA\UnifiedPushProvider\Utils;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;
use Psr\Container\ContainerInterface;

class AdminSettings implements ISettings {
	/** @var ContainerInterface */
	private ContainerInterface $context;
	/** @var RedisFactory */
	private RedisFactory $redisFactory;
	/** @var IDBConnection */
	private IDBConnection $db;

	/**
	 * @param ContainerInterface $context
	 * @param RedisFactory $redisFactory
	 * @param IDBConnection $db
	 */
	public function __construct(
		ContainerInterface $context,
		RedisFactory $redisFactory,
		IDBConnection $db
	) {
		$this->context = $context;
		$this->redisFactory = $redisFactory;
		$this->db = $db;
	}

	public function getForm(): TemplateResponse {
		$parameters = array();
		$redis_error = Utils::getRedisError($this->context);
		if ($redis_error !== null) {
			$parameters['error'] = $redis_error;
			return new TemplateResponse("uppush", "admin-redis-error", $parameters);
		}

		$parameters['keepalive'] = "50";
		$parameters['max_message_ttl'] = /*24 hours*/"86400";

		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from('uppush_config')
			->where($query->expr()->in('parameter', $query->createNamedParameter(array_keys($parameters), IQueryBuilder::PARAM_STR_ARRAY)));
		
		$result = $query->execute();
		while ($row = $result->fetch()){
			$parameters[$row['parameter']] = $row['value'];
		}
		$result->closeCursor();
		return new TemplateResponse("uppush", 'admin-settings', $parameters);
	}

	public function getSection() {
		return "uppush";
	}

	public function getPriority() {
		return 0;
	}
}
