<?php script("uppush", "admin"); ?>
<style>
#uppush-admin input:invalid {
	border: 2px solid red;
}

</style>
<div id="uppush-admin" class="section">
<h2><?php p($l->t("UnifiedPush Provider Settings")); ?></h2>
<p><?php p($l->t("The objective of this service is to make installation and configuration as easy as possible, but there are certain requirements that must be met for the system to work correctly:")); ?></p>
<br><div>
<ol>
<li><?php p($l->t("The service requires Redis to be installed and configured for use by Nextcloud.")); ?></li>
<li><?php echo $l->t('The maximum number of PHP processes must be set high enough. Each device connected for push messaging will need their own PHP process running. The parameter to adjust in your PHP-FPM configuration is %1$s.', ['<code>pm.max_children</code>'] ); ?></li>
<li><?php echo $l->t("Buffering must be disabled. This has to be configured on your reverse proxy."); ?></li>
<li><?php p($l->t("Your reverse proxy connection timeout must be higher than the one configured for this service. In order to reduce battery consumption, the timeout should be high enough (50s+, 300s+ if possible).")); ?></li>
</ol>
<br>
<form action=""><input type="number" min="15" max="600" required id="keepalive" placeholder="Keep-alive interval" value="<?php echo filter_var ( $_['keepalive'], FILTER_SANITIZE_NUMBER_INT); ?>";>
<button class="button" type="submit"><?php p($l->t("Set keep-alive")); ?></button>
</form>
<form onsubmit="setMaxMessageTtl"><input type="number" min="3600" max="259200" required id="max_message_ttl" placeholder="Max message TTL" value="<?php echo filter_var ( $_['max_message_ttl'], FILTER_SANITIZE_NUMBER_INT); ?>";>
<button class="button" type="submit"><?php p($l->t("Set max message TTL")); ?></button>
</form>
</div>
</div>
</div>
