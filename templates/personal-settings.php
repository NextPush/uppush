<style>
table td, table th {
	padding: 5px;
	word-break: break-all;
}
.uppush-date:not(.setted) {
    display: none;
}
</style>
<div id="uppush-inst" class="section">
<h2><?php p($l->t("UnifiedPush Provider")); ?></h2>
<br>
<p><?php echo $l->t('This service, along with the NextPush application on the device, allows notifying applications supporting %1$s, just like Firebase Cloud Messaging works with Google Play Service.',['<a style="color: lightblue; text-decoration: underline;"" href="https://unifiedpush.org">UnifiedPush</a>']); ?></p>
<br>
<p><?php p($l->t("The NextPush application source code and issue tracker can be found here:")); ?><br>
<a style="color: lightblue; text-decoration: underline;" href="https://codeberg.org/NextPush/nextpush-android">https://codeberg.org/NextPush/nextpush-android</a></p>
<br>
<br>
<a href="https://codeberg.org/NextPush/nextpush-android/releases"><button class="button"><?php p($l->t("Install from Codeberg Release")); ?></button></a>
<a href="https://f-droid.org/en/packages/org.unifiedpush.distributor.nextpush/"><button class="button"><?php p($l->t("Install from F-Droid")); ?></button></a>
<br>
</div>
<div id="uppush-auth" class="section">
<h2><?php p($l->t("Registered Devices")); ?></h2>
<p class="settings-hint"><?php p($l->t("List of registered devices and applications.")); ?></p>
    <ul>
<?php
$devices = $_['devices'];
$Application = $l->t("Application");
$Date = $l->t("Date");
$Delete = $l->t("Delete");
foreach($devices as $device) {
    $deviceName = filter_var($device['name'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $deviceDate = filter_var($device['date'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $deviceToken = filter_var($device['token'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    echo "
        <li id='li-".$deviceToken."'>".$deviceName." (<span class='uppush-date'>".$deviceDate."</span>) <button id='toggle-".$deviceToken."' class='toggle-device'>+</button> <button id='delete-".$deviceToken."' class='delete-device'>".$Delete."</button></li>
        <table cellpadding=3 id='table-".$deviceToken."' hidden>
            <thead>
                <tr>
                    <th></th>
                    <th><b>".$Application."</b></th>
                    <th><b>".$Date."</b></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
    ";
    $apps = $device['apps'];
    foreach($apps as $app) {
        $appName = filter_var($app['name'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $appDate = filter_var($app['date'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $appToken = filter_var($app['token'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        echo "
                <tr id='tr-".$appToken."'>
                    <th></th>
                    <th>".$appName."</th>
                    <th class='uppush-date'>".$appDate."</th>
                    <th><button id='delete-".$appToken."' class='delete-app'>".$Delete."</button></th>
                </tr>
        ";
    }
    echo "
                <tr><th> </th></tr>
            </tbody>
        </table>
";
}
script("uppush", "personal");
?>
    </ul>
