# Overview of the architecture

## Endpoints

* GET / => Check uppush
* PUT /keepalive/ => Set keepalive
* PUT /device/ => Add a device
* GET /device/{deviceId} => Sync incoming messages
* DELETE /message/{messageId} => Aknowledge or delete non-distributed push message
* DELETE /device/{deviceId} => Delete device
* PUT /app/ => Add new application
* DELETE /app/{token} => Delete application
* POST /push/{token} => Push new message
* GET /push/{token} => Discovery request
* GET /gateway/matrix => Gateway discovery request
* POST /gateway/matrix => Matrix push gateway (receive messages from matrix)

## Redis channels

### Variables used in the key

* `$deviceId`: Random string to identify a device
* `$messageId`: Random string to identify a message

### Channels

* `uppush.$deviceId.owner` => process owner of the device (uuid of the lock)
* `uppush.$deviceId.messages.very-low` => list of $messageId with very-low urgency
* `uppush.$deviceId.messages.low` => list of $messageId with low urgency
* `uppush.$deviceId.messages.normal` => list of $messageId with normal urgency
* `uppush.$deviceId.messages.high` => list of $messageId with high urgency
* `uppush.$deviceId.last_sent_id.very-low` => $messageId of the last message sent to the client with very-low urgency, used if the client doesn't send `LastEventId` header
* `uppush.$deviceId.last_sent_id.low` => $messageId of the last message sent to the client with low urgency, used if the client doesn't send `LastEventId` header
* `uppush.$deviceId.last_sent_id.normal` => $messageId of the last message sent to the client with normal urgency, used if the client doesn't send `LastEventId` header
* `uppush.$deviceId.last_sent_id.high` => $messageId of the last message sent to the client with high urgency, used if the client doesn't send `LastEventId` header
* `uppush.$deviceId.message.$messageId` => json_encoded event
* `uppush.$deviceId.topic.$topic` => $messageId of the last push message with $topic
* `uppush.$deviceId.notify` => list of dummy strings. When a process receives a new message it informs the sync process there are pending messages by pushing to this key.

## Locking/Unlocking

Lock classes have a $lock->token to identify themselves, this is a random string.

When a process _locks_ for a device, it adds its $lock->token to `uppush.$deviceId.owner` with redis SET command to be sure it is the only process accessing the device data.
If not refreshed (by calling _lock_ again), the lock timeout after 10 seconds.

When a process _unlocks_ for a device, it unlinks `uppush.$deviceId.owner` with redis UNLINK command.

## When a push message is received

1. The controller function `push` is called, with the app token $token:
	1. If `TTL` header is set, it takes the minimum value of `TTL` header or $maxMessageTtl for the time to keep message in redis, else the $maxMessageTtl (noted $ttl from now).
	2. It calls `_push` method which returns a $messageId (random string) if $token is known, else null.
	3. If $messageId is null, it returns 404, else it return 201, with headers: `TTL: $ttl` and `Location: /message/$messageId`
2. When `_push` is called, with the app token $token and the TTL $ttl:
	1. Retrieve the $deviceId from the $token, and return null if not found.
	2. Take the value of `Urgency` header if set (and == "very-low", "low", "normal" or "high"), else "normal" for the urgency to use (noted $urgency from now).
	3. Take the value of `Topic` header if set (noted $topic from now).
	4. Generate a $messageId for the message.
	5. Create an instance of `Event` (type: `Message`) containing the body base64_encoded, the $token, the $messageId, the $topic, the $urgency and the $ttl (if < 60, takes 60).
	6. Call Device->push($message)
3. When `Device->push` is called, with the Event $message:
	1. Lock the process for the device
	2. Set `uppush.$deviceId.message.$messageId` to json_encoded $message, using the redis SET command
	3. If $topic is not null:
		1. Set `uppush.$deviceId.topic.$topic` to $messageId and get the old value using the redis GETSET command
		2. If the $oldMessageId is not null, unlink `uppush.$deviceId.message.$oldMessageId` with the redis UNLINK command (we keep the messageId in the messages.$urgency lists)
	3. Add $messageId to `uppush.$deviceId.messages.$urgency` with the redis RPUSH command
	4. Add a dummy string to `uppush.$deviceId.notify`, so the sync process can starts when unlock
	5. Unlock the process for the device

## When the client syncs for incoming messages

1. The controller function `syncDevice` is called, with the $deviceId:
	1. Check if the $deviceId is known, or return 401
	2. Retrieve the $keepalive value configured
	3. Take the value of `LastEventId` and parse it to get and object `LastEventIds` containing $lastHighEventId, $lastNormalEventId, $lastLowEventId and $lastVeryLowEventId.
	4. Return some headers requires to keep connection alive behind some reverse proxy, and flush
	5. Call Device->sync($keepalive, $lastEventIds)
2. When `Device->sync` is called, with $keepalive and $lastEventIds:
	1. Send events `start` and `keepalive` and flush
	2. Lock the process for the device
	3. Get the index $lastHighEventIndex of $lastHighEventId:
        	1. Try to get the position of $lastHighEventId in `uppush.$deviceId.messages.high` with redis LPOS command
        	2. If not found get the value of `uppush.$deviceId.last_sent_id.high` with redis GET command, and replace $lastHighEventId with the value
        	3. If found try to get the position of $lastHighEventId in `uppush.$deviceId.messages.high` with redis LPOS command
        	4. If not found use the position 0
        4. Do the same for $lastNormalEventIndex, $lastLowEventIndex and $lastVeryLowEventIndex
        5. Get the list of events to get for `uppush.$deviceId.messages.high`, `uppush.$deviceId.messages.normal`, `uppush.$deviceId.messages.low` and `uppush.$deviceId.messages.very-low` with the redis LRANGE command, from $lastXXXXEventIndex to -1
        6. Replace the 4 $lastXXXXEventId with the last of each list.
        7. Get the events from `uppush.$deviceId.message.$messageId` with the messageId from the previous command, using the redis MGET command. Ignore all null messages.
        8. If the connection is healthy (connection_status != 0), set $lastXXXXEventIndex for the 4 urgency to `uppush.$deviceId.last_sent_id.$urgency` key with the redis SET command.
        9. Unlock the process for the device
	8. While the connection is healthy (connection_status != 0): wait for a notify event, with the redis BRBOP command for `uppush.$deviceId.notify`, during at most $timeout
	9. If no message arrived: send a ping,
	10. Else lock the process for the device
	11. Remove all pending notify event with the redis UNLINK command for `uppush.$deviceId.notify`
	12. Get the index $lastXXXXEventIndex of $lastXXXXEventId following steps 2.3.2->4 and repeat steps 2.4 to 2.9

TODO: ideally, if the connection is healthy, we should remove the content of messages older than 15min or 30min their message$Id from the lists

