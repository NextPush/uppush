#!/bin/bash

##
# Script to test nextpush.
# Usage : ./tests.sh http://your.domain.tld/
##

TEMP=$(mktemp -d /tmp/uppush.XXXXXXXX)

ok() {
    echo "[*] $@"
}

step() {
    echo "[+] $@"
}

error() {
    echo "[!] $@"
}

quit() {
    error "$@"
    exit 0
}


check_server() {
    step "Checking URL"
    curl -s $URL/status.php | grep Nextcloud >/dev/null || quit "Nextcloud server not found"
    ok "Nextcloud server found."
}

test_matrix_gateway() {
    step "Checking matrix gateway"
    curl -s $URL/index.php/apps/uppush/gateway/matrix | grep '{"unifiedpush":{"gateway":"matrix"}}' >/dev/null || error "Can't access non-setup matrix gateway."
    curl -s $URL/_matrix/push/v1/notify | grep '{"unifiedpush":{"gateway":"matrix"}}' >/dev/null
    if [ $? -ne 0 ]; then
        error "Matrix gateway is not setup. cf. https://codeberg.org/NextPush/uppush#gateways"
    else
        curl -s -X POST --data '{"notification":{"devices":[{"app_id":"abcd","pushkey":"abcd"}]}}' $URL/_matrix/push/v1/notify | grep '{"rejected":\["abcd"\]}' >/dev/null
        if [ $? -ne 0 ]; then
            error "Error while notifying to the matrix gateway"
        else
            ok "Matrix gateway correctly set up"
        fi
    fi
}

get_jwt_payload() {
    HEADER='{"typ":"JWT","alg":"ES256"}'
    BODY='{"aud":"'$URL'","exp":'$(date --date='1 hour' "+%s")'}'
    HEADER=$(echo -n "$HEADER" | basenc -w0 --base64url)
    BODY=$(echo -n "$BODY" | basenc -w0 --base64url)
    echo "$HEADER.$BODY" | sed 's/=//g'
}

asn1_to_raw() {
    openssl asn1parse -inform der | perl -n -e '
/INTEGER           :([0-9A-Z]{60})$/ && print "0000$1";
/INTEGER           :([0-9A-Z]{62})$/ && print "00$1";
/INTEGER           :([0-9A-Z]{64})$/ && print $1;
' | xxd -p -r
}

gen_vapid() {
    openssl ecparam -genkey -name prime256v1 -noout -out $TEMP/private.pem
    PUBKEY=$(openssl ec -in $TEMP/private.pem -pubout -conv_form uncompressed -outform DER | tail -c 65 | basenc -w0 --base64url | sed 's/=//g')
    JWT_PAYLOAD=$(get_jwt_payload)
    SIGNATURE=$(echo -n "$JWT_PAYLOAD" | openssl dgst -sha256 -sign $TEMP/private.pem | asn1_to_raw | basenc -w0 --base64url | sed 's/=//g')
    echo "vapid t=$JWT_PAYLOAD.$SIGNATURE,k=$PUBKEY"
}

test_sync() {
    step "Testing sync"
    echo -n "Username: "
    read USER
    echo -n "Password: "
    read -s PASSWORD
    echo

    step "Create device"
    DEVICE_ID=$(curl -s -u "$USER:$PASSWORD" -X PUT --data "deviceName=device-test" "$URL/index.php/apps/uppush/device/" | jq -r '.deviceId') || quit "Cannot create device"
    step "Create app"
    APP_ID=$(curl -s -u "$USER:$PASSWORD" -X PUT --data "deviceId=$DEVICE_ID&appName=app-test" "$URL/index.php/apps/uppush/app/" | jq -r '.token') || quit "Cannot create app"
    step "Sync"
    ( curl -s --max-time 3 "$URL/index.php/apps/uppush/device/$DEVICE_ID" & curl -s --data "TEST" "$URL/index.php/apps/uppush/push/$APP_ID" ) | grep "VEVTVA==" >/dev/null || quit "Cannot receive notification"
    ok "Synchronization worked"


    step "Create app with VAPID"
    VAPID=$(gen_vapid)
    PUBKEY=$(echo -n "$VAPID" | sed 's/.*k=//')
    APP_ID=$(curl -s -u "$USER:$PASSWORD" -X PUT --data "deviceId=$DEVICE_ID&appName=app-test-vapid&vapid=$PUBKEY" "$URL/index.php/apps/uppush/app/" | jq -r '.token') || quit "Cannot create app"
    step "Sync"
    ( curl -s --max-time 3 "$URL/index.php/apps/uppush/device/$DEVICE_ID" & curl -s --data "VAPID" -H "Authorization: $VAPID" "$URL/index.php/apps/uppush/push/$APP_ID" ) | grep "VkFQSUQ" >/dev/null || quit "Cannot receive notification"
    ok "Synchronization worked"
}

[ $# -eq 1 ] || quit "Usage: $0 http://your.domain.tld"

URL="$1"

check_server
test_matrix_gateway

echo "Testing sync ? [Yy/Nn]"
read sync
[ "$sync" == "y" ] || [ "$sync" == "Y" ] && test_sync
