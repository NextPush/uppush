#!/bin/bash

[ $# -ne 1 ] && echo "Usage: $0 {version}" && exit 1

VERSION="$1"

[ -f "$VERSION.tar.gz" ] || wget "https://codeberg.org/NextPush/uppush/archive/$VERSION.tar.gz"

pass ls | grep uppush.key >/dev/null
if [ $? -ne 0 ]; then
  echo "Pass uppush.key not found. Aborting."
  exit 1
fi

echo "Lien:"
echo "https://codeberg.org/NextPush/uppush/archive/$VERSION.tar.gz"
echo "Signature:"
openssl dgst -sha512 -sign <( pass uppush.key ) "$VERSION.tar.gz" | openssl base64

