## 2.3.0
- Add id, et translations
- Support Nextcloud 31

## 2.2.5
- Fix last table name during migration
- Add uk translations

## 2.2.4
- Drop old foreignkey during upgrade

## 2.2.3
- Change fk for the upgrade
- Fix DB for non-default prefixes

## 2.2.0
- Migrate db to a consistent state, should fix issues with the db.
- Add gl, it, zh_Hans translations
- Add CORS headers for OPTIONS requests, to allow sending webpush msgs from a web app
