<?php

namespace UPPUSH\Firebase\JWT;

class SignatureInvalidException extends \UnexpectedValueException
{
}
